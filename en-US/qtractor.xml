<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE bookinfo PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % BOOK_ENTITIES SYSTEM "Qtractor.ent">
%BOOK_ENTITIES;
]>

<part label="0">
  <title>
    Installing Qtractor
  </title>

  <partintro>
    <para>
      The different ways to install Qtractor and ensure your audio
      production environment is ready to make serious sounds.
    </para>
    <!-- insert literallayout here -->
    <literallayout>
       <xref linkend="qtractor-ch-01"/>
       <xref linkend="qtractor-ch-02"/>
       <xref linkend="qtractor-ch-03"/>
     </literallayout>
   </partintro>

<!-- actual real content starts here -->

  <chapter id="qtractor-ch-01">
    <title>
      Introduction to Qtractor
    </title>

  <para>
    <application>Qtractor</application> is a full-featured Digital
    Audio Workstation (DAW); a multi-track audio and MIDI recorder,
    editor, and mixer. Qtractor is free open-source software, licensed
    under the GPL, being developed by reknowned programmer Rui Nuno
    Capela.
  </para>

  <para>
    The functionality of <application>Qtractor</application> is
    contained within a graphical desktop environment that will be
    familiar to users of other popular multi-track
    recording&#47;editing applications on any computer operating
    system, and follows the same design principles with many of the
    same or similar elements.
  </para>

  <para>
    In addition to recording digital audio and MIDI,
    <application>Qtractor</application> provides an environment for
    multi-track clip-oriented composing techniques common in modern
    music-making and aims to be intuitive and easy to use, yet
    powerful enough for the serious recording enthusiast.
  </para>

  <note>
    <para>
      <application>Qtractor</application> is not what is known as a
      &#34;tracker&#34; type of audio&#47;MIDI application, although it has
      the potential to function in that way if needed. When used
      merely as an audio and&#47;or MIDI recorder (a MIDI recorder was
      historically called a &#34;sequencer&#34;) or arranger,
      <application>Qtractor</application> is non-destructive, which
      means that the underlying files that contain the audio or MIDI
      data are not altered when those files are apparently cut into
      pieces, duplicated, pulled or pasted into a different order in
      time, or manipulated in any number of ways within the main
      Window (GUI interface) of
      <application>Qtractor</application>. However, when used as an
      audio or MIDI recorder, for example, or when editing previously
      recorded MIDI data in the dedicated MIDI editor, Qtractor's
      actions can be destructive in the sense that newly recorded data
      (or altered MIDI data) replaces previously recorded data on the
      same track.
    </para>
  </note>

  <section id="qtractor-requirements">
    <title>
      System Requirements
    </title>
    
  <para>
    <application>Qtractor</application>'s target platform is
    <application>Linux</application>, using the
    <application>ALSA</application> (Advanced Linux Sound
    Architecture) and <application>JACK</application> (the Jack Audio
    Connection Kit) as the supporting infrastructure for recognizing
    sources of digital audio and MIDI (Musical Instrument Digital
    Interface) data, communicating with those sources and routing the
    data to and from various locations and programs (applications,
    including Qtractor) both inside and outside the computer and
    involving both software and hardware interfaces.
  </para>

  <para>
    Suggestions of technical requirements for <application>Qtractor</application> are:
  </para>
<indexterm>
  <primary>
    system requirements
  </primary>
  <secondary>
    hardware
  </secondary>
</indexterm>

  <itemizedlist>
    <listitem>
      <para>
	PC running a distribution of the GNU+Linux operating system
	with a Kernel of 2.6.38.4 or higher for best realtime
	performance (older kernels require realtime patches from your
	distribution or from kernel.org)
      </para>
    </listitem>

    <listitem>
      <para>
	CPU capable of running reasonably modern software (the better
	your CPU, the better performance you will see in realtime
	audio effects and synthesis)
      </para>
    </listitem>

    <listitem>
      <para>
	A reasonable amount of RAM (an open <application>Qtractor</application> project requires
	RAM; the more you have, the more complex your projects can be
	and the faster they will respond)
      </para>
    </listitem>

    <listitem>
      <para>
	A sound card; the basic sound card that shipped with your
	computer is perfectly acceptable, although if you require simultaneous
	recording of multiple tracks, a card or audio interface with
	distinct inputs will be required
      </para>
    </listitem>

    <listitem>
      <para>
	Ample harddrive space for samples and audio files used in your
	projects.
      </para>
    </listitem>

    <listitem>
      <para>
	Attitude. Not required, but if you plan on being a rock star,
	then you should try to develop an rebellious attitude, and
	resolutely reject mainstream software
      </para>
    </listitem>
  </itemizedlist>
  </section>

  <section id="qtractor-join">
      <title>
	Get Involved
      </title>

      <para>
	The <application>Qtractor</application> project welcomes all
	collaboration and review from the
	<application>Linux</application> audio developer and user
	community in particular, and the public in general.
      </para>

      <para>
	See <ulink
	url="http://qtractor.sourceforge.net">qtractor.sourceforge.net</ulink>
	for contact information.
      </para>

      <para>
	The docbook sources for this user manual are available via
	<application>git</application> from  <ulink
	url="http://gitlab.com/slackermedia">gitlab.com/slackermedia</ulink>
      </para>
  </section>

</chapter>
  
<!-- breaker -->

<chapter id="qtractor-ch-02">
  <title>
    Installation
  </title>
<indexterm>
  <primary>
    installation
  </primary>
  <secondary>
    qtractor
  </secondary>
</indexterm>

  <para>
    Installing <application>Qtractor</application> can be done in two ways: from your
    distribution's software repository, or by building from source
    code. For most users, installing from your distribution's
    repositories is the right choice, as it offers the easiest install
    and ensures timely and reliable updates.
  </para>

  <section id="qtractor-install">
    <title>
      Easy Install
    </title>

    <para>
      To install software from your distribution's repository, open
      the software installer for your distribution (often called
      <application>Add&#47;Remove Software</application> or sometimes
      a <application>Software Store</application>).
    </para>
    
    <para>
      Search for Qtractor, and mark it for installation and proceed;
      there is no need to intall additional packages since the
      installer will automatically install any software required for
      <application>Qtractor</application> to run.
    </para>

  </section>

  <section id="qtractor-expert-install">
    <title>
      Expert Install
    </title>

    <para>
      If you absolutely require the latest features in a just-released
      or not-yet released version of
      <application>Qtractor</application>, or you simply prefer to
      build your programs manually, you may build
      <application>Qtractor</application> from source code.
    </para>

    <para>
      In order to build from source, you must have a build
      environment, and the essential dependencies that
      <application>Qtractor</application> requires. Depending on your
      distribution, the names of the installable build tools will
      vary, but the software components themselves are:
    </para>

<indexterm>
  <primary>
    system requirements
  </primary>
  <secondary>
    software dependencies
  </secondary>
</indexterm>

    <itemizedlist>
      <listitem>
	<para>
	  <application>g++</application> - the GNU C++ Compiler
	</para>
      </listitem>

      <listitem>
	<para>
	  <application>gcc</application> - the GNU C Compiler; not
	  strictly required in this case, but good to have in a build
	  environment, and in some distributions this may bring in
	  other useful dependencies
	</para>
      </listitem>

      <listitem>
	<para>
	  <application>Autoconf</application> - 
	</para>
      </listitem>

      <listitem>
	<para>
	  <application>Automake</application> - the GNU Makefile generator
	</para>
      </listitem>

      <listitem>
	<para>
	  <application>Qt4</application> (core,gui,xml) - C++ class library and tools for
	  cross-platform development and internationalization,
	  available from your distribution's software repository (be
	  sure to install all &quot;development&quot; components) or
	  directly from <ulink
	  url="http://www.trolltech.org/products/qt/">trolltech.org/products/qt</ulink>
	</para>
      </listitem>

      <listitem>
	<para>
	  <application>libsndfile</application> - C library for reading and writing files
	  containing sampled sound, available from your distribution (be
	  sure to install all &quot;development&quot; components)
	  or directly from <ulink
	  url="http://www.mega-nerd.com/libsndfile/">mega-nerd.com/libsndfile</ulink>
	</para>
      </listitem>

      <listitem>
	<para>
	  <application>LADSPA</application> - Linux Audio Developer's
	  Simple Plugin API; available from you distribution's
	  repository (be
	  sure to install all &quot;development&quot; components) or from <ulink
	  url="http://www.ladspa.org/">ladspa.org</ulink>
	</para>
      </listitem>
    </itemizedlist>

    <para>
      Additional support libraries may be installed to enhance the
      abilities of <application>Qtractor</application>:
    </para>

  <itemizedlist>
    <listitem>
      <para>
	<application>libvorbis</application> - the Ogg Vorbis encoder
	and decoder by <ulink
	url="http://xiph.org/vorbis">Xiph</ulink>
      </para>
    </listitem>

    <listitem>
      <para>
	<application>libmad</application> - high-quality MPEG audio
	decoder, available from your repository or from <ulink
	url="http://www.underbit.com/products/mad/">underbit.com&#47;products&#47;mad</ulink>
      </para>
    </listitem>

    <listitem>
      <para>
	<application>libsameplerate</application> - the secret rabbit
	code, a C library for audio sample rate conversion, available
	from your distribution's repository or from
	<ulink url="http://www.mega-nerd.com/libsndfile/">mega-nerd.com&#47;libsndfile</ulink>
      </para>
    </listitem>

    <listitem>
      <para>
	<application>librubberband</application> - the Rubber Band
	Audio Time Stretcher, an audio time-stretching and
	pitch-shifting library, available from your repository or
	<ulink url="http://breakfastquay.com/rubberband/">breakfastquay.com&#47;rubberband</ulink>
      </para>
    </listitem>

    <listitem>
      <para>
	<application>liblo</application> - Lightweight OSC
	implementation (needed for DSSI GUI support), available from
	your repository or <ulink
	url="http://liblo.sourceforge.net/">liblo.sourceforge.net</ulink>
      </para>
    </listitem>

    <listitem>
      <para>
	<application>DSSI</application> - an API for soft synth
	plugins with custom user interfaces <ulink
	url="http://dssi.sourceforge.net">dssi.sourceforge.net</ulink>
      </para>
    </listitem>

    <listitem>
      <indexterm>
	<primary>
	  installation
	</primary>
	<secondary>
	  vst sdk
	</secondary>
      </indexterm>

      <para>
	<application>VST-SDK</application> - Steinberg's Virtual
	Studio Technology; this SDK permits the use of Linux-native
	plugins with Qtractor. To install VST-SDK:
      </para>

      <orderedlist>
	<listitem>
	  <para>
	  Establish a developer account at 
	  <ulink
	  url="http://www.steinberg.net/en/company/3rd_party_developer.html">
	  steinberg.net/en/company/3rd_party_developer.html
	</ulink>, confirm the registration, and log in
	  </para>
	</listitem>

	<listitem>
	  <para>
	    Download VST 2.4
	  </para>
	</listitem>

	<listitem>
	  <para>
	    Unzip the source directory and copy the files to the
	    appropriate directory; usually
	    &#47;usr&#47;local&#47;include
	  </para>

	  <programlisting>
	    su -c 'cp vstsdk2.4/pluginterfaces/vst2.x/aeffect*
      /usr/local/include/'
	  </programlisting>
	</listitem>
      </orderedlist> <!-- closes vst_sdk install subroutine -->
    </listitem> <!-- closes vst sdk listitem-->
  </itemizedlist> <!-- closes optional install subroutine --> 

  <section id="qtractor-source">
    <title>
      Download the Qtractor Code
    </title>
<indexterm>
  <primary>
    installation
  </primary>
  <secondary>
    from source
  </secondary>
</indexterm>

    <para>
      Stable releases of <application>Qtractor</application> can be
      downloaded directly from 
      <ulink url="http://qtractor.sourceforge.net/qtractor-index.html#Downloads">
	qtractor.sourceforge.net&#47;qtractor-index.html&#35;Downloads
      </ulink>
    </para>

    <note>
      <para>
	If you intend to develop <application>Qtractor</application>
	then you will want to run &quot;bleeding edge&quot; code. This
	is not recommended for a production system and should be used
	only for development purposes.  Check out the source code from
	its CVS repository via anonymous (pserver) access:
      </para>

      <orderedlist>
	<listitem>
	  <para>
	    At the command line, checkout the code via SVN:
	  </para>
	  <programlisting>
	    svn co https&#58;&#47;&#47;qtractor.svn.sourceforge.net&#47;svnroot&#47;qtractor&#47;trunk qtractor-svn
	  </programlisting>
	</listitem>

	<listitem>
	  <para>
	    Prepare the configure script on the newly created qtractor
	    source tree directory:
	  </para>
	  <programlisting>
	    cd qtractor-svn
	    make -f Makefile.svn 
	  </programlisting>
	</listitem>

	<listitem>
	  <para>
	    Build and install.
	  </para>
	</listitem>
      </orderedlist>
    </note>
  </section>

  <section id="qtractor-compile">
    <title>
      Compiling and Installing Qtractor
    </title>

    <para>
      After downloading Qtractor, decompress and extract the archive:
    </para>
    
    <programlisting>
      tar -xf qtractor-0.x.x.tar.gz
    </programlisting>

    <para>
      Change directory into the resulting
      <filename>qtractor</filename> directory. Once inside the
      <filename>qtractor</filename> directory, the usual building
      commands apply:
    </para>

    <programlisting>
      ./configure &amp;&amp; make 
    </programlisting>

    <para>
      To see all configuration options before entering the command
      sequence above, type <userinput>./configure
      &#45;&#45;help</userinput> 
    </para>

    <para>
      After typing the <userinput>configure</userinput> and
      <userinput>make</userinput> commands and waiting until the program
      has finished being compiled, become an administrator of your
      system (using either the <userinput>sudo</userinput> or
      <userinput>su</userinput> command to become, temporarily, the
      <userinput>root</userinput> user) and finish the installation by
      entering:
    </para>

    <programlisting>
      make install 
    </programlisting>

    <para>
      The executable binary code (in otherwords, the
      <application>Qtractor</application> application itself) and
      associated desktop and icon files are copied to common, standard
      system locations.
    </para>
  </section> <!-- end build and install routine -->
  </section>  <!-- end Expert Install section -->

</chapter> <!-- end Install chapter -->

<chapter id="qtractor-ch-03">
  <title>
    Post-Installation Optimizations
  </title>

  <para>
    There are some considerations after you have installed
    <application>Qtractor</application> which may help you optimize
    your system for its best possible audio performance.
  </para>

<indexterm>
  <primary>
    realtime kernel
  </primary>
</indexterm>

  <para>
    Previous to the Linux Kernel 2.6.38.4, a realtime kernel was a
    must-have component in audio production. Since 2.6.38, new options
    have developed to provide a seamless audio production experience,
    depending largely on what your needs are from your system.
  </para>

  <para>
    If you are not sure whether you'll need realtime, it is safe to
    assume that you will need either:
  </para>

    <itemizedlist>
      <listitem>
	<para>
	  A 2.6.38.4 or greater kernel regardless of your expected workload
	</para>
      </listitem>

      <listitem>
	<para>
	  A realtime kernel if a 2.6.38.4 or better kernel is not conveniently
	  available from your distribution
	</para>
      </listitem>

      <listitem>
	<para>
	  An optimized realtime kernel if you anticipate live recording and multitracking
	</para>
      </listitem>
    </itemizedlist>

  <section id="qtractor-kernel">
    <title>
      Real-Time Kernel
    </title>
    
    <para>
      To discover what kernel your computer is currently running,
      look in KInfoCenter (in KDE4) or System Information (in
      Gnome3). 
    </para>

    <mediaobject>
      <imageobject>
	<imagedata fileref="./images/kinfocenter.png" format="PNG"/>
      </imageobject>
      <textobject>
	<phrase>KInfoCenter</phrase>
      </textobject>
    </mediaobject>

    <para>
     Or simply open a terminal:
    </para>

    <programlisting>
      uname -r
    </programlisting>

   <para>
      If your computer is running a kernel previous to 2.6.38.4, then
      you should either update your kernel, or install a realtime
      version of your kernel.
    </para>

    <para>
      To upgrade the Linux kernel, use your distribution's repository,
      searching for the terms <userinput>realtime</userinput> or
      <userinput>rt</userinput>.
    </para>

    <para>
      If your distribution does not have repositories, or realtime
      kernels, then you can of course download the
      source code and compile the kernel yourself. The same guiding
      principles apply; as long as you are building 2.6.38.4 or above,
      then a simple <userinput>make oldconfig</userinput> will render
      a modestly pre-emptive kernel suitable for near-realtime
      use. For extreme low-latency, apply realtime patches, available
      from <ulink
      url="http://www.kernel.org/pub/linux/kernel/projects/rt/">
      kernel.org&#47;pub&#47;linux&#47;kernel&#47;projects&#47;rt
    </ulink>
    </para>
      
  </section>

<!-- breaker -->

  <section id="qtractor-qjackctl">
    <title>
      QJackCtl
    </title>

    <para>
      In order for <application>Qtractor</application> to run,
      <application>JACK</application> must be running in the
      background. <application>JACK</application> is a kind of
      patchbay for Linux (technically a &quot;sound server&quot;),
      enabling you to route sound in and out of
      <application>Qtractor</application> and other sound
      applications.
    </para>

    <para>
      Some distributions may configure <application>Qtractor</application>
      such that <application>JACK</application> starts automatically
      when <application>Qtractor</application> is launched; others may
      not. If you launch <application>Qtractor</application> from your
      application menu and receive <application>JACK</application>
      errors in the message log at the bottom of the
      <application>Qtractor</application> window, then quit
      <application>Qtractor</application> and start
      <application>JACK</application> manually.
    </para>

    <para>
      The easiest way to start <application>JACK</application> is with
      <application>QJackCtl</application> (&quot;Q JACK
      Control&quot;), a control panel and global, synchronized
      timecode display. If this is not installed, install it from your
      distribution's repository or from <ulink
      url="http://qjackctl.sourceforge.net">
      qjackctl.sourceforge.net
      </ulink>
    </para>

    <para>
      If necessary, launch <application>QJackCtl</application> and
      click the <guibutton>Start</guibutton> button. 
    </para>
  
    <mediaobject>
      <imageobject>
	<imagedata fileref="./images/qjackctl.png" format="PNG"/>
      </imageobject>
      <textobject>
	<phrase>QJackQtl</phrase>
      </textobject>
    </mediaobject>

    <para>
      This should instate realtime privileges and begin a global,
      synchronized timecode clock that will allow
      <application>Qtractor</application> to use all available inputs
      and outputs just like a mixing board would in the studio.
    </para>

    <para>
      To view the status of <application>JACK</application>, click the
      <guibutton>Status</guibutton> button and review the resulting
      report. <application>JACK</application> should be reported as
      running, in realtime mode. 
    </para>

    <mediaobject>
      <imageobject>
	<imagedata fileref="./images/qjackctl_status.png" format="PNG"/>
      </imageobject>
      <textobject>
	<phrase>QJackQtl Status window</phrase>
      </textobject>
    </mediaobject>
 
<section id="qtractor-troubleshooting">
  <title>
    Troubleshooting
  </title>

<indexterm>
  <primary>
    troubleshooting
  </primary>
</indexterm>

    <para>
      If you are experiencing unexpected results with
      <application>QJackCtl</application>, then here are some things
      to check:
    </para>

    <itemizedlist>
      <listitem>
<indexterm>
  <primary>
    realtime kernel
  </primary>
  <secondary>
    troubleshooting
  </secondary>
</indexterm>

	<para>
	  <emphasis>
	    <application>JACK</application> does not indicate realtime
	    mode in the Status window, on a system using the
	    <application>PAM</application> security model.
	  </emphasis>
	</para>

	<orderedlist>
	  <listitem>
	    <para>
	      Click the <guibutton>Setup</guibutton> button to open
	      preferences. Activate <guilabel>Realtime</guilabel> in
	      the left column, and then quit
	      <application>QJackCtl</application> while you configure
	      your system further.
	    </para>
	  </listitem>

	  <listitem>
	    <para>
	      Make sure you have a realtime-capable (ie, 2.6.38.4 or
	      greater, or an RT-optimized kernel of any variety) kernel installed and that
	      applications have permission to utilize the realtime
	      mode. On distributions using
	      <application>PAM</application>, look in
	      <filename>
		&#47;etc&#47;security&#47;limits.conf
	      </filename>
	      and ensure that there is a user group being granted
	      realtime (or <userinput>rtprio</userinput> in
	      <application>PAM</application> terminology) privileges.
	    </para>
	    <para>
	      If <userinput>rtprio</userinput> is being granted to a
	      group, then you should make sure that you are in that
	      group. For instance, if the group
	      <userinput>audio</userinput> is being granted realtime
	      privileges then execute <userinput>cat
	      &#47;etc&#47;group | grep audio</userinput> and look to
	      see that your userinput is listed as a member of the
	      audio group. If not, execute <userinput>usermod -a -G audio
	      youruserinput</userinput> as root to add your user to the
	      audio group.
	    </para>
	  
	    <warning>
	      <para>
		If <filename>limits.conf</filename> does not exist,
		then your system does not use
		<application>PAM</application>.
	      </para>
	    </warning>

	    <para>
	      If no group is given realtime priority, create a group
	      and add yourself to the group. This must be done with
	      root privileges:
	    </para>

	    <programlisting>
	      groupadd realtime
	      usermod -a -G realtime yourUserName
	    </programlisting>

	    <para>
	      Then add realtime priority permissions to the group (and
	      accordingly any user in that group):
	    </para>

	    <programlisting>
	      @realtime       hard    rtprio          20
	      @realtime       soft    rtprio          10
	    </programlisting>
	  </listitem>

<!-- new trouble issue -->

      <listitem>
	<indexterm>
	  <primary>
	    realtime kernel
	  </primary>
	  <secondary>
	    troubleshooting
	  </secondary>
	</indexterm>

	<para>
	  <emphasis>
	    <application>JACK</application> does not indicate realtime
	    mode in the Status window, on a system <emphasis>not</emphasis> using the
	    <application>PAM</application> security model.
	  </emphasis>
	</para>

	<orderedlist>
	  <listitem>
	    <para>
	      Click the <guibutton>Setup</guibutton> button to open
	      preferences. Activate <guilabel>Realtime</guilabel> in
	      the left column, and then restart
	      <application>QJackCtl</application> while you configure
	      your system further.
	    </para>
	  </listitem>

	  <listitem>
	    <indexterm>
	      <primary>
		set_rlimits
	      </primary>
	    </indexterm>

	    <para>
	      To enable realtime priority for your user applications,
	      install <application>set_rlimits</application> from your
	      repository or directly from
	      <ulink url="http://www.physics.adelaide.edu.au/~jwoithe/">
		physics.adelaide.edu.au&#47;~jwoithe
	      </ulink>
	    </para>
	  </listitem>

	  <listitem>
	    <para>
	      As root, open
	      <filename>&#47;etc&#47;set_rlimits.conf.new</filename>
	      in your favourite text editor. Add two lines:
	    </para>

	    <programlisting>
	      @audio  &#47;usr&#47;local&#47;bin&#47;jackd  nice=-1  rtprio=80
	      memlock=100000
	      @audio  &#47;usr&#47;local&#47;bin&#47;qtractor  nice=-1  rtprio=80
	      memlock=100000
	    </programlisting>
	    
	    <para>
	      Save the file as
	      <filename>&#47;etc&#47;set_rlimits.conf</filename>
	    </para>
	  </listitem>

	  <listitem>
	    <para>
	      Create a custom launcher so that when you start your
	      realtime applications, they are started in the syntax
	      <userinput>set_rlimits qtractor</userinput>.
	    </para>

	    <para>
	      In KDE, do
	      this by right-clicking on the <guibutton>K
	      Menu</guibutton> and edit the launch commands for
	      <application>QJackCtl</application> and
	      <application>Qtractor</application>. 
	    </para>

	    <para>
	      In Gnome, edit as root the <filename>.desktop</filename>
	      file for the applications (ie, 
	      <filename>&#47;usr&#47;share&#47;applications&#47;qtractor.desktop</filename>)
	      such that the <userinput>Exec=</userinput> line executes
	      <userinput>set_rlimits qtractor</userinput>
	    </para>
	  </listitem>
	</orderedlist>
      </listitem>

<!-- new trouble issue -->

      <listitem>
	<para>
	  <emphasis>
	    <application>JACK</application> does not start when
	    <application>Qtractor</application> is launched.
	  </emphasis>
	</para>

	<orderedlist>
	  <listitem>
	    <para>
	      Your distribution may not have configured
	      <application>JACK</application> to automatically start
	      upon <application>Qtractor</application>'s
	      launch.
	    </para>
	  </listitem>

	  <listitem>
	    <para>
	      The easiest workaround for this is to launch
	      <application>QJackCtl</application> manually first, and
	      then launch <application>Qtractor</application>.
	    </para>
	  </listitem>
	</orderedlist>
      </listitem>
	</orderedlist>
      </listitem>
    </itemizedlist>
</section>
  </section>
</chapter>
</part>

<!-- breaker -->

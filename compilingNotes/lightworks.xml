<chapter id="lightworks">
<title>Lightworks</title>

<indexterm>
  <primary>
    video editing
  </primary>
</indexterm>

<highlights>
    <variablelist>
      <title>
	Strengths
      </title>

      <varlistentry>
	<term>
	  Powerful
	</term>

	<listitem>
	  <para>
	    All the essential video editing functions are present, in a professional environment&#59; in and out points, clip monitors and dedicated timeline monitors, copious effects, clip bins, splicing, sliding, slipping, titling, a full proxy system, auto saves, and much more.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>
	  Industry-Grade
	</term>

	<listitem>
	  <para>
	    If you are used to working in the industry on professianl editing workstations or a Steenbeck flatbed editor, this is unquestionably what you are seeking. <application>Lightworks</application> takes an editing suite and puts it into your Linux box, complete with timecode- and track- based editing styles, extensive media management options, stable and high-quality effects, EDL import and export, support for Final Cut and Avid format exchanges, and an over-all lack of bloat that distracts from its streamlined purpose.
	  </para>
</listitem>
      </varlistentry>

      <varlistentry>
	<term>
	  Documentation
	</term>

	<listitem>
	  <para>
	    Very good video tutorials for <application>Lightworks</application> are available from Editshare, for free, on youtube.com.
	  </para>
	</listitem>	    
      </varlistentry>

      
      <varlistentry>
	<term>
	  Stability
	</term>

	<listitem>
	  <para>
	    <application>Lightworks</application> is focused and streamlined, tried and proven. It is a stable editor, so much so that it is nearly a kiosk. Start it, and live in its environment all day.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>

    <variablelist>
      <title>
	Weaknesses
      </title>

      <varlistentry>
	<term>
	  Open Source..?
	</term>

	<listitem>
	  <para>
	    <application>Lightworks</application> has been around for three decades and has been used on a wide variety of films, and although it was announced that it would be open sourced a few years ago, the actual source code itself has so far not surfaced. If you are only interested in finding a company in which you can have confidence that it will not deprecate and abandon your project file formats for years to come, then <application>Lightworks</application> is as good as some closed source NLEs and quite a lot better than others. If you are looking far on open source solution, then this is not yet the solution for you, although it is still on the official <application>Lightworks</application> roadmap to formally release the codebase &#40;no word yet on what license it will use&#41;.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>
	  Complex
	</term>

	<listitem>
	  <para>
	    If you are not used to traditional film editing, <application>Lightworks</application> may confuse you initially. There will be a learning curve.
	  </para>
	</listitem>
      </varlistentry>

      <varlistentry>
	<term>
	  Registration
	</term>

	<listitem>
	  <para>
	    Registration is required to use <application>Lightworks</application>. It is free to register, but you do have to sign in.
	  </para>
	</listitem>
      </varlistentry>

      
      <varlistentry>
	<term>
	  Conservatively Restrictive
	</term>

	<listitem>
	  <para>
	    Although it has full access to the same robust multimedia libraries that everything else does, <application>Lightworks</application> only permits you to import a set of codecs, and export even a smaller set. This makes it slightly more predictable than an NLE that lets anything in, but it is a little restrictive by comparison.
	  </para>
	</listitem>
      </varlistentry>


      <varlistentry>
	<term>
	  64-bit Only
	</term>

	<listitem>
	  <para>
	    Currently, <application>Lightworks</application> is for 64-bit architecture only, so if you are running a 32-bit install, you cannot run it.
	  </para>
	</listitem>
      </varlistentry>


      <varlistentry>
	<term>
	  Heavyweight
	</term>

	<listitem>
	  <para>
	    <application>Lightworks</application> is not a lightweight or simple application. It officially requires an Nvidia GPU running proprietary drivers &#40;in practise, it can be run on a good Intel graphics chipset, although it performs better on a proper GPU&#41;, at least 3GB RAM &#40;you want at least 8GB in practise&#41;, and even suggests running the application from a separate drive than where your media is located to minimise lag. It is intended to be an edititng station, not a lightweight video app.
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
</highlights>


<sidebar>
  <title>See Also</title>
  <simplelist type='vert' columns='1'>
    <member>Kdenlive</member>
    <member>Blender</member>
    <member>Flowblade</member>
  </simplelist>
</sidebar>

<para>
  <application>Lightworks</application> is a video editing application aimed at the professional video editor. It is designed with a traditional film editing environment in mind and has been compared to non-digital platforms like the Steenbeck. While some NLE systems encourage &#34;lifting&#34; clips out of the edit to adjust them, or conversely to drop all clips into the timeline and use that space as a work table, <application>Lightworks</application> treats the timeline as a working draft of the completed project, and encourages users to unjoin splices just as one would unjoin splicing tape on real film, and adjust clips with slide and roll edits. It can be an efficient and technical way of editing, but to people who learnt editing by trial and error, it there is, realistically, a learning curve.
</para>

<para>
  The main advantage to <application>Lightworks</application> for the Linux user is that it is, all else being equal, inarguably the most polished and reliable option in terms of a complete professional editor. If you are looking for an NLE that works, which you can install and move on with your life, then <application>Lightworks</application>, combined with capable hardware, will put an end to your search. Lesser NLE-for-Linux attempts pale in comparison&#59; it is probably the only NLE on Linux that will convince a doubtful prospective user that Linux can be a serious video editing solution &#40;albeit in part by sheer intimidation of the application itself&#41;.
</para>

<para>
  This is not a sales pitch for <application>Lightworks</application>, however, and you should use whatever suits your own workflow. In fact, <application>Lightworks</application> is not quite open source yet &#40;there is an as-yet undelivered promise to release its code&#41;, so if you want to use only free and open source software, then opt for something else.
</para>

<para>
  To install <application>Lightworks</application>, use Slackermedia&#39;s SlackBuild script, available from <ulink url="http://slackbuilds.org">slackbuilds.org</ulink>.
</para>

<para>
  It is required that you create a <application>Lightworks</application> account and sign into <application>Lightworks</application> in order to run the application, so make sure you at least launch <application>Lightworks</application> with an internet connection after installing it. After this initial launch, you do not need internet access for <application>Lightworks</application> ever again.
</para>

<warning>
  <para>
    Editshare, the owner and proprietor of <application>Lightworks</application>, emails a newsletter to all account holders by default, and obviously Slackermedia cannot guarantee that they do not sell email addresses to advertisers. You may want to use a junk-mail email address or an alias account or a temporary address from a service like <ulink url="http://10minutemail.com">10minutemail.com</ulink>, or whatever you do to manage spam.
  </para>
</warning>

<para>
  <application>Lightworks</application> can be used for free, but there are also &#34;pro&#34; features &#40;user defined project locations, sharing, timeline rendering, stereoscopic output, advanced export options&#41; available for either a monthly subscription fee or for yearly or version-buyout plans. For comparisons between the different support options, see <ulink url="http://www.lwks.com">lwks.com</ulink>.
</para>

<para>
  A very comprehensive set of tutorials from Editshare are available for free on their YouTube channel, <ulink url="https://www.youtube.com/user/editshare">https&#58;&#47;&#47;youtube.c&#47;m/user&#47;editshare</ulink>.
</para>

</chapter>

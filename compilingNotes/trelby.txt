Chapter 1.  Trelby

Strengths

Solid

    Formats screenplays as you write, with all the common features of a
    screenplay authoring application.

GPL

    An open licensed application that relies on open source technology. Unlike
    competitors, it makes no attempt to lock you out of your own data, or to
    sell a cloud subscription to you.

See Also

Screenwriter
Fountain

A free word processor for screenplays, Trelby is an application with all the
usual features of a screenwriting application, including a character name
database, basic scene reporting, PDF exporting, and more.

The advantage to using a dedicated screenwriting application is that all
standard formatting is applied automatically for you, with minimal amounts of
interaction. Character names are remembered for you and auto-completion
simplifies concerns, for instance, about whether you called that day player
"Evil Henchman" or just "Henchman".

Of all the applications in multimedia, screenwriting applications are the least
likely to require cross-compatibility. Unless you are collaborating with
another writer, you will probably never send out the project file. Since Trelby
can export to PDF, it's probably everything you need in a screenwriting
application.

Trelby is also Fountain compatible.

Trelby is a Python application and can be installed from slackbuilds.org.


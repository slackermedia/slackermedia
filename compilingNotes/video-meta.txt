video-meta

-------------------------------------------------------------------------------

Mike Schilli's perl script to show metadata on a video file is a helpful "video
forensics" tool that invokes the Video::FrameGrab module which in turn invokes 
mplayer. This tool can quickly help a video artist determine codecs, frame
sizes, bitrates, frame rates and other properties of video clips at a glance.
The installation is easy but unique in that it involves perl, which has its own
way of installing modules. It also depends upon yaml, available from
SlackBuilds.org

 1. Install the video::framegrab perl module with the command:

          $ su -c "perl -MCPAN -e
          'install Video::FrameGrab'"


 2. Install the data::dump perl module with the command:

          $ su -c "perl -MCPAN -e
          'install Data::Dump'"


 3. The script video-meta itself requires no installation, although the initial
    line of the script needs modifying so that the script can find perl on the
    system.

          $ wget
          ftp://linuxmagazin.de/pub/magazin/2010/01/Perl/video-meta -O video-meta.pl
          $ sed "1s#/local##"
          video-meta.pl > video-meta
          $ chmod +x video-meta
          $ su -c 'cp video-meta /usr/local/bin/'
        


The script can then be used by simply invoking it before the name of a video
file, such as: video-meta foo.avi, which will list all metadata attributes
associated with foo.avi

Mediainfo

Providing you with even more information about media is mediainfo and 
mediainfo-gui. Obviously one works in the shell and one with a GUI. The amount
of useful information provided by mediainfo is unparalleled, and so it should
be on every multimedia artist's system.

Installation of mediainfo and its dependency libmediainfo can be done easily
through slackbuilds.org


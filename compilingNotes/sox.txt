SoX

-------------------------------------------------------------------------------

The default Slackware install of SoX lacks MP3 support, presumably for legal
reasons. As transcode is to video, so SoX is to audio; therefore, to have
maximum flexibility in your multimedia studio you will want to create a custom
compile of SoX.

Note

If you don't feel like re-compiling Sox is worth the trouble, you can probably
do much of the typical SoX tasks (such as transcoding, sample rate conversion,
and so on) with ffmpeg.

Then download the sox source code and Pat's SlackBuild script from your local
Slackware mirror, and rebuild with as much codec support as you can manage. For
example:

 ./configure --with-distro='Slackermedia13'
--with-ladspa-path='/usr/lib64/' --with-oggvorbis=dyn
--with-flac=dyn --with-amrwb=dyn --with-amrnb=dyn --with-wavpack=dyn
--with-alsa=dyn --with-ffmpeg=dyn --with-oss=dyn --with-sndfile=dyn
--with-mp3=dyn --with-gsm=dyn --with-lpc10=dyn --with-ao=dyn
--libdir='/usr/lib64'
--mandir='/usr/man/'

Once your new version of SoX is compiled, install it using the upgradepkg
command>:

  * If you built a newer version of SoX than the one that shipped with
    Slackware, then issue the command

          # upgradepkg /tmp/sox-x.x*t?z


    Where x.x is the newer version number.

  * If you built the same version of SoX as the one that shipped with
    Slackware, then issue the command:

          # upgradepkg --reinstall /tmp/sox-x.x*t?z



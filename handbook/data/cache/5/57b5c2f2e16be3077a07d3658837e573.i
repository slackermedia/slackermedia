a:49:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:"Chapter 1.  Darktable";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:24;}i:4;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:24;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Strengths";}i:2;i:26;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:35;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:35;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:16:"Photo Management";}i:2;i:37;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:53;}i:10;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:279:"Digital photography has left most people with thousands of photos that they
never look at because they cannot begin to organise them all. Darktable is
a sensible organisational environment. Yes, you could just organise photos
in your file manager, but Darktable makes more sense.";}i:2;i:53;}i:11;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:350;}i:12;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:350;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:"Non-Destructive Editing";}i:2;i:352;}i:14;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:375;}i:15;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:176:"Darktable creates a stack of filters through which you may view and export
your images. Filters can be modified or removed independently of the source
image and of one another.";}i:2;i:375;}i:16;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:565;}i:17;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:565;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"Weaknesses";}i:2;i:567;}i:19;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:577;}i:20;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:577;}i:21;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"Complex";}i:2;i:579;}i:22;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:586;}i:23;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:142:"This is an application filled with advanced, professional-level
photographic features, so it takes time to learn it entirely and to master
it.";}i:2;i:586;}i:24;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:742;}i:25;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:742;}i:26;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:"Non-Destructive Editing";}i:2;i:744;}i:27;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:767;}i:28;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:244:"The non-destructive, non-linear workflow is not for everyone. There are
those who prefer to make a manual backup of an image, and then experiment
with effects and hands-on bitmap tools. Darktable is not the best solution
for that style of work.";}i:2;i:767;}i:29;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:1029;}i:30;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1029;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"See Also";}i:2;i:1031;}i:32;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1039;}i:33;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1039;}i:34;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:"Digikam
GIMP
Shotwell";}i:2;i:1041;}i:35;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1062;}i:36;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1062;}i:37;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:589:"Darktable takes a new approach to the digital darkroom paradigm. Digikam and 
GIMP, for example, both make changes to the data that you load into them; when
you change the colour balance of a photograph, these applications re-write
pixels to reflect the change (assuming you save the image; otherwise it's done
in RAM only). If you want multiple versions of the same image, such as a colour
version of a photograph as well as a black-and-white version, then you must
copy the source data and maintain, literally, two files (or two layers in one
file, at best). Darktable uses filters only.";}i:2;i:1064;}i:38;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1653;}i:39;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1653;}i:40;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:347:"Think of Darktable as a lens that you place over a photograph; one filter might
make a colour photograph black-and-white, while another might make the colours
brighter and more vivid. One filter might sharpen an image and another might
diffuse or blur it. In fact, a Darktable lens can also rotate or crop, balance
colours, stylize, and much more.";}i:2;i:1655;}i:41;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2002;}i:42;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2002;}i:43;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:336:"Because the filters are filters, they do not change the source file itself. You
can re-order the filters, modify their properties, remove a few, and so on, all
without the need to undo the filters that you already applied. The workflow is
non-linear because the data from the source photo is exactly the same through
the entire process.";}i:2;i:2004;}i:44;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2340;}i:45;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2340;}i:46;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:91:"There is a build script available on Slackbuilds.org which should suit most
people's needs.";}i:2;i:2342;}i:47;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2342;}i:48;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:2342;}}
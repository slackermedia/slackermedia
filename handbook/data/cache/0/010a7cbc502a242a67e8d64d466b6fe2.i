a:57:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:"Chapter 1.  Digikam";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:22;}i:4;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:22;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Strengths";}i:2;i:24;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:33;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:33;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:16:"Photo Management";}i:2;i:35;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:51;}i:10;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:275:"Digital photography has left most people with thousands of photos that they
never look at because they cannot begin to organise them all. Digikam is a
sensible organisational environment. Yes, you could just organise photos in
your file manager, but Digikam makes more sense.";}i:2;i:51;}i:11;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:344;}i:12;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:344;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"Configurable";}i:2;i:346;}i:14;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:358;}i:15;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:183:"The KDE family is know for configurability, and Digikam is no exception.
They're your photographs, so you deal with them anyway you want; Digikam
will help, but never get in your way.";}i:2;i:358;}i:16;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:555;}i:17;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:555;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"Weaknesses";}i:2;i:557;}i:19;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:567;}i:20;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:567;}i:21;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"Complex";}i:2;i:569;}i:22;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:576;}i:23;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:204:"This is an application filled with features and configuration options, so
it takes time to learn it entirely, and then more time to decide which
features you want to use and which ones you want to ignore.";}i:2;i:576;}i:24;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:794;}i:25;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:794;}i:26;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:26:"No Non-Destructive Editing";}i:2;i:796;}i:27;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:822;}i:28;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:161:"Compared to Darktable, which configures non-linear filters through which
you may view and export those images, Digikam modifies the pixels of the
image linearly.";}i:2;i:822;}i:29;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:997;}i:30;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:997;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"See Also";}i:2;i:999;}i:32;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1007;}i:33;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1007;}i:34;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:"Darktable
GIMP
Shotwell";}i:2;i:1009;}i:35;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1032;}i:36;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1032;}i:37;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:453:"Digikam is a direct competitor to powerful digital darkroom applications as
well as to photo managers. It is fully featured, powerful, and has a rich
plugin structure which enables the addition of even more features as the need
arises (such as quick uploaders to popular online photo shares, new filters,
and so on). Even if a graphic artist or photographer is happy using GIMP, 
digikam is a useful tool for its workflow and image manipulation ability.";}i:2;i:1034;}i:38;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1487;}i:39;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1487;}i:40;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:"An ebook with Digikam ";}i:2;i:1489;}i:41;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:1511;}i:42;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"recipes";}i:2;i:1512;}i:43;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:1519;}i:44;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:75:" is available from one of the developers:
dmpop.dhcp.io%#!digikamrecipes.md";}i:2;i:1520;}i:45;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1595;}i:46;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1595;}i:47;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:255:"While GIMP concentrates on enabling an artist to open an image for re-touching,
compositing, and general manipulation, Digikam's goal is to organize
photographs, making them easy to see as an overview of a photoshoot or life
events and to choose the best ";}i:2;i:1597;}i:48;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:1852;}i:49;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"take";}i:2;i:1853;}i:50;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:1857;}i:51;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:395:" out of multiple images. It can also do
image correction and even has a robust set of plugins and filters such that it
approaches some of more interesting features of GIMP. However, it does not try
to emulate or replace GIMP and is not meant for serious compositing, such as
combining parts of two almost-perfect photographs to make one perfect shot, or
adding text or elaborate lighting effecs.";}i:2;i:1858;}i:52;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2253;}i:53;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2253;}i:54;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:44:"Digikam is installable from slackbuilds.org.";}i:2;i:2255;}i:55;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2299;}i:56;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:2299;}}
a:33:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:"Chapter 1.  HandBrake";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:24;}i:4;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:24;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"See Also";}i:2;i:26;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:34;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:34;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"dvd::rip
k3b";}i:2;i:36;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:48;}i:10;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:48;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:31:"HandBrake is a commandline and ";}i:2;i:50;}i:12;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"GUI";}i:2;i:81;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:262:" DVD ripper that will read DVDs and blu-ray
and convert them to some other format, like Ogg Theora, webm, xvid, x264, and
others. It is one of the many invaluable tools for the video editor, who can
typically expect to receive video from every imaginable source.";}i:2;i:84;}i:14;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:346;}i:15;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:346;}i:16;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:98:"HandBrake can also be used as a simple format converter, transcoding one file
into another format.";}i:2;i:348;}i:17;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:446;}i:18;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:446;}i:19;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:425:"HandBrake is available as a binary install from slackware.org.uk/people/alien/
restricted_slackbuilds/handbrake or you can compile your own copy from
slackbuilds.org, although the way that the HandBrake build system works does
not make it valuable to compile from source, so Slackermedia recommends using
the binary install (in spite of the fact that the maintainer for the SlackBuild
is also the maintainer of Slackermedia).";}i:2;i:448;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:873;}i:21;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:873;}i:22;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Important";}i:2;i:875;}i:23;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:884;}i:24;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:884;}i:25;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:470:"HandBrake requires very specific codec versions in order to build correctly, so
the SlackBuild downloads all of the HandBrake-sanctioned codec versions even
though you may already have all of those codecs installed. The SlackBuild is
not installing or re-installing these codecs onto your system, but using them
to compile HandBrake. Those versions of the codecs are discarded after 
HandBrake is finished building. Install the binary package instead! it's much
simpler.";}i:2;i:886;}i:26;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1356;}i:27;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1356;}i:28;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:78:"Once installed, invoke HandBrake in a terminal as HandBrakeCLI and within the
";}i:2;i:1358;}i:29;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:3:"GUI";}i:2;i:1436;}i:30;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:" as only HandBrake.";}i:2;i:1439;}i:31;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1458;}i:32;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1458;}}
a:809:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:17:"Formatting Syntax";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:13:"doku>DokuWiki";i:1;N;i:2;s:4:"doku";i:3;s:8:"DokuWiki";}i:2;i:34;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:237:" supports some simple markup language, which tries to make the datafiles to be as readable as possible. This page contains all possible syntax you may use when editing the pages. Simply have a look at the source of this page by pressing ";}i:2;i:51;}i:6;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:288;}i:7;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"Edit this page";}i:2;i:289;}i:8;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:303;}i:9;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:45:". If you want to try something, just use the ";}i:2;i:304;}i:10;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:21:"playground:playground";i:1;s:10:"playground";}i:2;i:349;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:51:" page. The simpler markup is easily accessible via ";}i:2;i:385;}i:12;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:12:"doku>toolbar";i:1;s:12:"quickbuttons";i:2;s:4:"doku";i:3;s:7:"toolbar";}i:2;i:436;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:", too.";}i:2;i:465;}i:14;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:471;}i:15;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:473;}i:16;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:21:"Basic Text Formatting";i:1;i:2;i:2;i:473;}i:2;i:473;}i:17;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:473;}i:18;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:473;}i:19;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:"DokuWiki supports ";}i:2;i:508;}i:20;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:1;i:1;s:2:"**";}i:2;i:1;i:3;s:2:"**";}i:2;i:526;}i:21;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:3;i:1;s:4:"bold";}i:2;i:3;i:3;s:4:"bold";}i:2;i:528;}i:22;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:4;i:1;s:2:"**";}i:2;i:4;i:3;s:2:"**";}i:2;i:532;}i:23;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:", ";}i:2;i:534;}i:24;a:3:{i:0;s:13:"emphasis_open";i:1;a:0:{}i:2;i:536;}i:25;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"italic";}i:2;i:538;}i:26;a:3:{i:0;s:14:"emphasis_close";i:1;a:0:{}i:2;i:544;}i:27;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:", ";}i:2;i:546;}i:28;a:3:{i:0;s:14:"underline_open";i:1;a:0:{}i:2;i:548;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"underlined";}i:2;i:550;}i:30;a:3:{i:0;s:15:"underline_close";i:1;a:0:{}i:2;i:560;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:" and ";}i:2;i:562;}i:32;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:567;}i:33;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"monospaced";}i:2;i:569;}i:34;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:579;}i:35;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:26:" texts. Of course you can ";}i:2;i:581;}i:36;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:1;i:1;s:2:"**";}i:2;i:1;i:3;s:2:"**";}i:2;i:607;}i:37;a:3:{i:0;s:14:"underline_open";i:1;a:0:{}i:2;i:609;}i:38;a:3:{i:0;s:13:"emphasis_open";i:1;a:0:{}i:2;i:611;}i:39;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:613;}i:40;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"combine";}i:2;i:615;}i:41;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:622;}i:42;a:3:{i:0;s:14:"emphasis_close";i:1;a:0:{}i:2;i:624;}i:43;a:3:{i:0;s:15:"underline_close";i:1;a:0:{}i:2;i:626;}i:44;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:4;i:1;s:2:"**";}i:2;i:4;i:3;s:2:"**";}i:2;i:628;}i:45;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:" all these.";}i:2;i:630;}i:46;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:642;}i:47;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:133:"DokuWiki supports **bold**, //italic//, __underlined__ and ''monospaced'' texts.
Of course you can **__//''combine''//__** all these.";}i:2;i:642;}i:48;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:642;}i:49;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"You can use ";}i:2;i:782;}i:50;a:3:{i:0;s:14:"subscript_open";i:1;a:0:{}i:2;i:794;}i:51;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"subscript";}i:2;i:799;}i:52;a:3:{i:0;s:15:"subscript_close";i:1;a:0:{}i:2;i:808;}i:53;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:" and ";}i:2;i:814;}i:54;a:3:{i:0;s:16:"superscript_open";i:1;a:0:{}i:2;i:819;}i:55;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"superscript";}i:2;i:824;}i:56;a:3:{i:0;s:17:"superscript_close";i:1;a:0:{}i:2;i:835;}i:57;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:", too.";}i:2;i:841;}i:58;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:848;}i:59;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:65:"You can use <sub>subscript</sub> and <sup>superscript</sup>, too.";}i:2;i:848;}i:60;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:848;}i:61;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:26:"You can mark something as ";}i:2;i:918;}i:62;a:3:{i:0;s:12:"deleted_open";i:1;a:0:{}i:2;i:944;}i:63;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"deleted";}i:2;i:949;}i:64;a:3:{i:0;s:13:"deleted_close";i:1;a:0:{}i:2;i:956;}i:65;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:" as well.";}i:2;i:962;}i:66;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:972;}i:67;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:53:"You can mark something as <del>deleted</del> as well.";}i:2;i:972;}i:68;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:972;}i:69;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:1;i:1;s:2:"**";}i:2;i:1;i:3;s:2:"**";}i:2;i:1030;}i:70;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:3;i:1;s:10:"Paragraphs";}i:2;i:3;i:3;s:10:"Paragraphs";}i:2;i:1032;}i:71;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:4;i:1;s:2:"**";}i:2;i:4;i:3;s:2:"**";}i:2;i:1042;}i:72;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:46:" are created from blank lines. If you want to ";}i:2;i:1044;}i:73;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:1;i:1;s:2:"**";}i:2;i:1;i:3;s:2:"**";}i:2;i:1090;}i:74;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:3;i:1;s:15:"force a newline";}i:2;i:3;i:3;s:15:"force a newline";}i:2;i:1092;}i:75;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:4;i:1;s:2:"**";}i:2;i:4;i:3;s:2:"**";}i:2;i:1107;}i:76;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:94:" without a paragraph, you can use two backslashes followed by a whitespace or the end of line.";}i:2;i:1109;}i:77;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1203;}i:78;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1203;}i:79;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:38:"This is some text with some linebreaks";}i:2;i:1205;}i:80;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:1243;}i:81;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:70:"Note that the
two backslashes are only recognized at the end of a line";}i:2;i:1246;}i:82;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:1316;}i:83;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"
or followed by";}i:2;i:1318;}i:84;a:3:{i:0;s:9:"linebreak";i:1;a:0:{}i:2;i:1333;}i:85;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:39:"a whitespace \\this happens without it.";}i:2;i:1336;}i:86;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1376;}i:87;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:170:"This is some text with some linebreaks\\ Note that the
two backslashes are only recognized at the end of a line\\
or followed by\\ a whitespace \\this happens without it.";}i:2;i:1376;}i:88;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1376;}i:89;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:53:"You should use forced newlines only if really needed.";}i:2;i:1555;}i:90;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1608;}i:91;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1610;}i:92;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:5:"Links";i:1;i:2;i:2;i:1610;}i:2;i:1610;}i:93;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:1610;}i:94;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1610;}i:95;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:50:"DokuWiki supports multiple ways of creating links.";}i:2;i:1629;}i:96;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1679;}i:97;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1681;}i:98;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:8:"External";i:1;i:3;i:2;i:1681;}i:2;i:1681;}i:99;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:1681;}i:100;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1681;}i:101;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:45:"External links are recognized automagically: ";}i:2;i:1701;}i:102;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:21:"http://www.google.com";i:1;N;}i:2;i:1746;}i:103;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:" or simply ";}i:2;i:1767;}i:104;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:21:"http://www.google.com";i:1;s:14:"www.google.com";}i:2;i:1778;}i:105;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:38:" - You can set the link text as well: ";}i:2;i:1792;}i:106;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:21:"http://www.google.com";i:1;s:26:"This Link points to google";}i:2;i:1830;}i:107;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:33:". Email addresses like this one: ";}i:2;i:1882;}i:108;a:3:{i:0;s:9:"emaillink";i:1;a:2:{i:0;s:19:"andi@splitbrain.org";i:1;N;}i:2;i:1915;}i:109;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:" are recognized, too.";}i:2;i:1936;}i:110;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1958;}i:111;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:303:"DokuWiki supports multiple ways of creating links. External links are recognized
automagically: http://www.google.com or simply www.google.com - You can set
link text as well: [[http://www.google.com|This Link points to google]]. Email
addresses like this one: <andi@splitbrain.org> are recognized, too.";}i:2;i:1958;}i:112;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:2272;}i:113;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:8:"Internal";i:1;i:3;i:2;i:2272;}i:2;i:2272;}i:114;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:2272;}i:115;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2272;}i:116;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:80:"Internal links are created by using square brackets. You can either just give a ";}i:2;i:2292;}i:117;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:8:"pagename";i:1;N;}i:2;i:2372;}i:118;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:" or use an additional ";}i:2;i:2384;}i:119;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:8:"pagename";i:1;s:9:"link text";}i:2;i:2406;}i:120;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:2428;}i:121;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2430;}i:122;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:137:"Internal links are created by using square brackets. You can either just give
a [[pagename]] or use an additional [[pagename|link text]].";}i:2;i:2430;}i:123;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2430;}i:124;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:13:"doku>pagename";i:1;s:14:"Wiki pagenames";i:2;s:4:"doku";i:3;s:8:"pagename";}i:2;i:2574;}i:125;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:78:" are converted to lowercase automatically, special characters are not allowed.";}i:2;i:2606;}i:126;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2684;}i:127;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2684;}i:128;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"You can use ";}i:2;i:2686;}i:129;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:15:"some:namespaces";i:1;N;}i:2;i:2698;}i:130;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:34:" by using a colon in the pagename.";}i:2;i:2717;}i:131;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2752;}i:132;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:65:"You can use [[some:namespaces]] by using a colon in the pagename.";}i:2;i:2752;}i:133;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2752;}i:134;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:33:"For details about namespaces see ";}i:2;i:2822;}i:135;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:15:"doku>namespaces";i:1;N;i:2;s:4:"doku";i:3;s:10:"namespaces";}i:2;i:2855;}i:136;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:2874;}i:137;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2875;}i:138;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2875;}i:139;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:112:"Linking to a specific section is possible, too. Just add the section name behind a hash character as known from ";}i:2;i:2877;}i:140;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:2989;}i:141;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:16:". This links to ";}i:2;i:2993;}i:142;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:15:"syntax#internal";i:1;s:12:"this Section";}i:2;i:3009;}i:143;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:3041;}i:144;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3043;}i:145;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:47:"This links to [[syntax#internal|this Section]].";}i:2;i:3043;}i:146;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3043;}i:147;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"Notes:";}i:2;i:3095;}i:148;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3101;}i:149;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:3101;}i:150;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:3101;}i:151;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:3101;}i:152;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:1;i:3;s:6:"

  * ";}i:2;i:3101;}i:153;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Links to ";}i:2;i:3107;}i:154;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:9:"Links to ";}i:2;i:3107;}i:155;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:6:"syntax";i:1;s:14:"existing pages";}i:2;i:3116;}i:156;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:37:" are shown in a different style from ";}i:2;i:3141;}i:157;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:37:" are shown in a different style from ";}i:2;i:3141;}i:158;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:11:"nonexisting";i:1;N;}i:2;i:3178;}i:159;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:" ones.";}i:2;i:3193;}i:160;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:6:" ones.";}i:2;i:3193;}i:161;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:3199;}i:162;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:3199;}i:163;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:3199;}i:164;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:3199;}i:165;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:2;i:3;s:5:"
  * ";}i:2;i:3199;}i:166;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:"DokuWiki does not use ";}i:2;i:3204;}i:167;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:22:"DokuWiki does not use ";}i:2;i:3204;}i:168;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:12:"wp>CamelCase";i:1;N;i:2;s:2:"wp";i:3;s:9:"CamelCase";}i:2;i:3226;}i:169;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:83:" to automatically create links by default, but this behavior can be enabled in the ";}i:2;i:3242;}i:170;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:83:" to automatically create links by default, but this behavior can be enabled in the ";}i:2;i:3242;}i:171;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:11:"doku>config";i:1;N;i:2;s:4:"doku";i:3;s:6:"config";}i:2;i:3325;}i:172;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:54:" file. Hint: If DokuWiki is a link, then it's enabled.";}i:2;i:3340;}i:173;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:54:" file. Hint: If DokuWiki is a link, then it's enabled.";}i:2;i:3340;}i:174;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:3394;}i:175;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:3394;}i:176;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:3394;}i:177;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:3394;}i:178;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:2;i:3;s:5:"
  * ";}i:2;i:3394;}i:179;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:106:"When a section's heading is changed, its bookmark changes, too. So don't rely on section linking too much.";}i:2;i:3399;}i:180;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:106:"When a section's heading is changed, its bookmark changes, too. So don't rely on section linking too much.";}i:2;i:3399;}i:181;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:3505;}i:182;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:3505;}i:183;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:3505;}i:184;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:3505;}i:185;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3507;}i:186;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:9:"Interwiki";i:1;i:3;i:2;i:3507;}i:2;i:3507;}i:187;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:3507;}i:188;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3507;}i:189;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:"DokuWiki supports ";}i:2;i:3528;}i:190;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:14:"doku>Interwiki";i:1;N;i:2;s:4:"doku";i:3;s:9:"Interwiki";}i:2;i:3546;}i:191;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:106:" links. These are quick links to other Wikis. For example this is a link to Wikipedia's page about Wikis: ";}i:2;i:3564;}i:192;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:7:"wp>Wiki";i:1;N;i:2;s:2:"wp";i:3;s:4:"Wiki";}i:2;i:3670;}i:193;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:3681;}i:194;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3683;}i:195;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:154:"DokuWiki supports [[doku>Interwiki]] links. These are quick links to other Wikis.
For example this is a link to Wikipedia's page about Wikis: [[wp>Wiki]].";}i:2;i:3683;}i:196;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:3844;}i:197;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:14:"Windows Shares";i:1;i:3;i:2;i:3844;}i:2;i:3844;}i:198;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:3844;}i:199;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3844;}i:200;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"Windows shares like ";}i:2;i:3870;}i:201;a:3:{i:0;s:16:"windowssharelink";i:1;a:2:{i:0;s:14:"\\server\share";i:1;s:4:"this";}i:2;i:3890;}i:202;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:106:" are recognized, too. Please note that these only make sense in a homogeneous user group like a corporate ";}i:2;i:3913;}i:203;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:11:"wp>Intranet";i:1;N;i:2;s:2:"wp";i:3;s:8:"Intranet";}i:2;i:4019;}i:204;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:4034;}i:205;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4036;}i:206;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:64:"Windows Shares like [[\\server\share|this]] are recognized, too.";}i:2;i:4036;}i:207;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4036;}i:208;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"Notes:";}i:2;i:4105;}i:209;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4111;}i:210;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:4111;}i:211;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:4111;}i:212;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:4111;}i:213;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:1;i:3;s:6:"

  * ";}i:2;i:4111;}i:214;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:126:"For security reasons direct browsing of windows shares only works in Microsoft Internet Explorer per default (and only in the ";}i:2;i:4117;}i:215;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:126:"For security reasons direct browsing of windows shares only works in Microsoft Internet Explorer per default (and only in the ";}i:2;i:4117;}i:216;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:4243;}i:217;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"local zone";}i:2;i:4244;}i:218;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:10:"local zone";}i:2;i:4244;}i:219;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:4254;}i:220;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:").";}i:2;i:4255;}i:221;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:2:").";}i:2;i:4255;}i:222;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:4257;}i:223;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:4257;}i:224;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:4257;}i:225;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:4257;}i:226;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:2;i:3;s:5:"
  * ";}i:2;i:4257;}i:227;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:88:"For Mozilla and Firefox it can be enabled through different workaround mentioned in the ";}i:2;i:4262;}i:228;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:88:"For Mozilla and Firefox it can be enabled through different workaround mentioned in the ";}i:2;i:4262;}i:229;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:58:"http://kb.mozillazine.org/Links_to_local_pages_do_not_work";i:1;s:22:"Mozilla Knowledge Base";}i:2;i:4350;}i:230;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:156:". However, there will still be a JavaScript warning about trying to open a Windows Share. To remove this warning (for all users), put the following line in ";}i:2;i:4435;}i:231;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:156:". However, there will still be a JavaScript warning about trying to open a Windows Share. To remove this warning (for all users), put the following line in ";}i:2;i:4435;}i:232;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:4591;}i:233;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:"conf/userscript.js";}i:2;i:4593;}i:234;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:4611;}i:235;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:":";}i:2;i:4613;}i:236;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:1:":";}i:2;i:4613;}i:237;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4614;}i:238;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"  LANG.nosmblinks = ";}i:2;i:4616;}i:239;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:20:"  LANG.nosmblinks = ";}i:2;i:4616;}i:240;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:4636;}i:241;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:135:";

==== Image Links ====

You can also use an image to link to another internal or external page by combining the syntax for links and ";}i:2;i:4638;}i:242;a:3:{i:0;s:9:"locallink";i:1;a:2:{i:0;s:22:"images_and_other_files";i:1;s:6:"images";}i:2;i:4773;}i:243;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:27:" (see below) like this:

  ";}i:2;i:4807;}i:244;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:18:"http://www.php.net";i:1;a:8:{s:4:"type";s:13:"internalmedia";s:3:"src";s:21:"wiki:dokuwiki-128.png";s:5:"title";N;s:5:"align";N;s:5:"width";N;s:6:"height";N;s:5:"cache";s:5:"cache";s:7:"linking";s:7:"details";}}i:2;i:4834;}i:245;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"

";}i:2;i:4882;}i:246;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:18:"http://www.php.net";i:1;a:8:{s:4:"type";s:13:"internalmedia";s:3:"src";s:21:"wiki:dokuwiki-128.png";s:5:"title";N;s:5:"align";N;s:5:"width";N;s:6:"height";N;s:5:"cache";s:5:"cache";s:7:"linking";s:7:"details";}}i:2;i:4884;}i:247;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:101:"

Please note: The image formatting is the only formatting syntax accepted in link names.

The whole ";}i:2;i:4932;}i:248;a:3:{i:0;s:9:"locallink";i:1;a:2:{i:0;s:22:"images_and_other_files";i:1;s:5:"image";}i:2;i:5033;}i:249;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:" and ";}i:2;i:5066;}i:250;a:3:{i:0;s:9:"locallink";i:1;a:2:{i:0;s:5:"links";i:1;s:4:"link";}i:2;i:5071;}i:251;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:154:" syntax is supported (including image resizing, internal and external images and URLs and interwiki links).

===== Footnotes =====

You can add footnotes ";}i:2;i:5086;}i:252;a:3:{i:0;s:4:"nest";i:1;a:1:{i:0;a:3:{i:0;a:3:{i:0;s:13:"footnote_open";i:1;a:0:{}i:2;i:5240;}i:1;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:"This is a footnote";}i:2;i:5242;}i:2;a:3:{i:0;s:14:"footnote_close";i:1;a:0:{}i:2;i:5260;}}}i:2;i:5240;}i:253;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:55:" by using double parentheses.

  You can add footnotes ";}i:2;i:5262;}i:254;a:3:{i:0;s:4:"nest";i:1;a:1:{i:0;a:3:{i:0;a:3:{i:0;s:13:"footnote_open";i:1;a:0:{}i:2;i:5317;}i:1;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:"This is a footnote";}i:2;i:5319;}i:2;a:3:{i:0;s:14:"footnote_close";i:1;a:0:{}i:2;i:5337;}}}i:2;i:5317;}i:255;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:221:" by using double parentheses.

===== Sectioning =====

You can use up to five different levels of headlines to structure your content. If you have more than three headlines, a table of contents is generated automatically ";}i:2;i:5339;}i:256;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:5560;}i:257;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:46:" this can be disabled by including the string ";}i:2;i:5562;}i:258;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:5608;}i:259;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"<nowiki>";}i:2;i:5610;}i:260;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:8:"<nowiki>";}i:2;i:5610;}i:261;a:3:{i:0;s:5:"notoc";i:1;a:0:{}i:2;i:5618;}i:262;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"</nowiki>";}i:2;i:5627;}i:263;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:9:"</nowiki>";}i:2;i:5627;}i:264;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:5636;}i:265;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:240:" in the document.

==== Headline Level 3 ====
=== Headline Level 4 ===
== Headline Level 5 ==

  ==== Headline Level 3 ====
  === Headline Level 4 ===
  == Headline Level 5 ==

By using four or more dashes, you can make a horizontal line:

";}i:2;i:5638;}i:266;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"---";}i:2;i:5878;}i:267;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:66:"-

===== Media Files =====

You can include external and internal ";}i:2;i:5881;}i:268;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:11:"doku>images";i:1;s:30:"images, videos and audio files";i:2;s:4:"doku";i:3;s:6:"images";}i:2;i:5947;}i:269;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:102:" with curly brackets. Optionally you can specify the size of them.

Real size:                        ";}i:2;i:5993;}i:270;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;N;i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6095;}i:271;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:36:"

Resize to given width:            ";}i:2;i:6120;}i:272;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;N;i:3;s:2:"50";i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6156;}i:273;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:34:"

Resize to given width and height";}i:2;i:6184;}i:274;a:3:{i:0;s:4:"nest";i:1;a:1:{i:0;a:3:{i:0;a:3:{i:0;s:13:"footnote_open";i:1;a:0:{}i:2;i:6218;}i:1;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:136:"when the aspect ratio of the given width and height doesn't match that of the image, it will be cropped to the new ratio before resizing";}i:2;i:6220;}i:2;a:3:{i:0;s:14:"footnote_close";i:1;a:0:{}i:2;i:6356;}}}i:2;i:6218;}i:275;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:": ";}i:2;i:6358;}i:276;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;N;i:3;s:3:"200";i:4;s:2:"50";i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6360;}i:277;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:36:"

Resized external image:           ";}i:2;i:6392;}i:278;a:3:{i:0;s:13:"externalmedia";i:1;a:7:{i:0;s:33:"http://de3.php.net/images/php.gif";i:1;N;i:2;N;i:3;s:3:"200";i:4;s:2:"50";i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6428;}i:279;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:38:"

  Real size:                        ";}i:2;i:6472;}i:280;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;N;i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6510;}i:281;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:37:"
  Resize to given width:            ";}i:2;i:6535;}i:282;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;N;i:3;s:2:"50";i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6572;}i:283;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:37:"
  Resize to given width and height: ";}i:2;i:6600;}i:284;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;N;i:3;s:3:"200";i:4;s:2:"50";i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6637;}i:285;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:37:"
  Resized external image:           ";}i:2;i:6669;}i:286;a:3:{i:0;s:13:"externalmedia";i:1;a:7:{i:0;s:33:"http://de3.php.net/images/php.gif";i:1;N;i:2;N;i:3;s:3:"200";i:4;s:2:"50";i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6706;}i:287;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:69:"


By using left or right whitespaces you can choose the alignment.

";}i:2;i:6750;}i:288;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;s:5:"right";i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6819;}i:289;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"

";}i:2;i:6845;}i:290;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;s:4:"left";i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6847;}i:291;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"

";}i:2;i:6873;}i:292;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;s:6:"center";i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6875;}i:293;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"

  ";}i:2;i:6902;}i:294;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;s:5:"right";i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6906;}i:295;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"
  ";}i:2;i:6932;}i:296;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;s:4:"left";i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6935;}i:297;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"
  ";}i:2;i:6961;}i:298;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;N;i:2;s:6:"center";i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:6964;}i:299;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:82:"

Of course, you can add a title (displayed as a tooltip by most browsers), too.

";}i:2;i:6991;}i:300;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;s:19:"This is the caption";i:2;s:6:"center";i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:7073;}i:301;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"

  ";}i:2;i:7120;}i:302;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:21:"wiki:dokuwiki-128.png";i:1;s:19:"This is the caption";i:2;s:6:"center";i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:7124;}i:303;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:43:"

For linking an image to another page see ";}i:2;i:7171;}i:304;a:3:{i:0;s:9:"locallink";i:1;a:2:{i:0;s:11:"Image Links";i:1;N;}i:2;i:7214;}i:305;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:112:" above.

==== Supported Media Formats ====

DokuWiki can embed the following media formats directly.

| Image | ";}i:2;i:7230;}i:306;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7342;}i:307;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"gif";}i:2;i:7344;}i:308;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:"gif";}i:2;i:7344;}i:309;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7347;}i:310;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:", ";}i:2;i:7349;}i:311;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7351;}i:312;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"jpg";}i:2;i:7353;}i:313;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:"jpg";}i:2;i:7353;}i:314;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7356;}i:315;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:", ";}i:2;i:7358;}i:316;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7360;}i:317;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"png";}i:2;i:7362;}i:318;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:"png";}i:2;i:7362;}i:319;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7365;}i:320;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"  |
| Video | ";}i:2;i:7367;}i:321;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7381;}i:322;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"webm";}i:2;i:7383;}i:323;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:4:"webm";}i:2;i:7383;}i:324;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7387;}i:325;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:", ";}i:2;i:7389;}i:326;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7391;}i:327;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"ogv";}i:2;i:7393;}i:328;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:"ogv";}i:2;i:7393;}i:329;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7396;}i:330;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:", ";}i:2;i:7398;}i:331;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7400;}i:332;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"mp4";}i:2;i:7402;}i:333;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:"mp4";}i:2;i:7402;}i:334;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7405;}i:335;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:13:" |
| Audio | ";}i:2;i:7407;}i:336;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7420;}i:337;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"ogg";}i:2;i:7422;}i:338;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:"ogg";}i:2;i:7422;}i:339;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7425;}i:340;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:", ";}i:2;i:7427;}i:341;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7429;}i:342;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"mp3";}i:2;i:7431;}i:343;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:"mp3";}i:2;i:7431;}i:344;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7434;}i:345;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:", ";}i:2;i:7436;}i:346;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7438;}i:347;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"wav";}i:2;i:7440;}i:348;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:"wav";}i:2;i:7440;}i:349;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7443;}i:350;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"  |
| Flash | ";}i:2;i:7445;}i:351;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7459;}i:352;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"swf";}i:2;i:7461;}i:353;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:"swf";}i:2;i:7461;}i:354;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7464;}i:355;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:388:"                    |

If you specify a filename that is not a supported media format, then it will be displayed as a link instead.

==== Fallback Formats ====

Unfortunately not all browsers understand all video and audio formats. To mitigate the problem, you can upload your file in different formats for maximum browser compatibility.

For example consider this embedded mp4 video:

  ";}i:2;i:7466;}i:356;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:9:"video.mp4";i:1;s:13:"A funny video";i:2;N;i:3;N;i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:7854;}i:357;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"

When you upload a ";}i:2;i:7881;}i:358;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7901;}i:359;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"video.webm";}i:2;i:7903;}i:360;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:10:"video.webm";}i:2;i:7903;}i:361;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7913;}i:362;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:" and ";}i:2;i:7915;}i:363;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7920;}i:364;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"video.ogv";}i:2;i:7922;}i:365;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:9:"video.ogv";}i:2;i:7922;}i:366;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7931;}i:367;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:" next to the referenced ";}i:2;i:7933;}i:368;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:7957;}i:369;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"video.mp4";}i:2;i:7959;}i:370;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:9:"video.mp4";}i:2;i:7959;}i:371;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:7968;}i:372;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:151:", DokuWiki will automatically add them as alternatives so that one of the three files is understood by your browser.

Additionally DokuWiki supports a ";}i:2;i:7970;}i:373;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:8121;}i:374;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"poster";}i:2;i:8122;}i:375;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:8128;}i:376;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:169:" image which will be shown before the video has started. That image needs to have the same filename as the video and be either a jpg or png file. In the example above a ";}i:2;i:8129;}i:377;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:8298;}i:378;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"video.jpg";}i:2;i:8300;}i:379;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:9:"video.jpg";}i:2;i:8300;}i:380;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:8309;}i:381;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:149:" file would work.

===== Lists =====

Dokuwiki supports ordered and unordered lists. To create a list item, indent your text by two spaces and use a ";}i:2;i:8311;}i:382;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:8460;}i:383;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"*";}i:2;i:8462;}i:384;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:1:"*";}i:2;i:8462;}i:385;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:8463;}i:386;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:26:" for unordered lists or a ";}i:2;i:8465;}i:387;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:8491;}i:388;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"-";}i:2;i:8493;}i:389;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:1:"-";}i:2;i:8493;}i:390;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:8494;}i:391;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:455:" for ordered ones.

  * This is a list
  * The second item
    * You may have different levels
  * Another item

  - The same list but ordered
  - Another item
    - Just use indention for deeper levels
  - That's it

<code>
  * This is a list
  * The second item
    * You may have different levels
  * Another item

  - The same list but ordered
  - Another item
    - Just use indention for deeper levels
  - That's it
</code>

Also take a look at the ";}i:2;i:8496;}i:392;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:14:"doku>faq:lists";i:1;s:17:"FAQ on list items";i:2;s:4:"doku";i:3;s:9:"faq:lists";}i:2;i:8951;}i:393;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:125:".

===== Text Conversions =====

DokuWiki can convert certain pre-defined characters or strings into images or other text or ";}i:2;i:8987;}i:394;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:9112;}i:395;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:76:".

The text to image conversion is mainly done for smileys. And the text to ";}i:2;i:9116;}i:396;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:9192;}i:397;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:84:" conversion is used for typography replacements, but can be configured to use other ";}i:2;i:9196;}i:398;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:9280;}i:399;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:80:" as well.

==== Text to Image Conversions ====

DokuWiki converts commonly used ";}i:2;i:9284;}i:400;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:11:"wp>emoticon";i:1;N;i:2;s:2:"wp";i:3;s:8:"emoticon";}i:2;i:9364;}i:401;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:40:"s to their graphical equivalents. Those ";}i:2;i:9379;}i:402;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:12:"doku>Smileys";i:1;N;i:2;s:4:"doku";i:3;s:7:"Smileys";}i:2;i:9419;}i:403;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:108:" and other images can be configured and extended. Here is an overview of Smileys included in DokuWiki:

  * ";}i:2;i:9435;}i:404;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:"8-)";}i:2;i:9543;}i:405;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9546;}i:406;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  8-)  ";}i:2;i:9549;}i:407;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9558;}i:408;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:"8-O";}i:2;i:9563;}i:409;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9566;}i:410;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  8-O  ";}i:2;i:9569;}i:411;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9578;}i:412;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-(";}i:2;i:9583;}i:413;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9586;}i:414;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-(  ";}i:2;i:9589;}i:415;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9598;}i:416;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-)";}i:2;i:9603;}i:417;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9606;}i:418;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-)  ";}i:2;i:9609;}i:419;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9618;}i:420;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:2:"=)";}i:2;i:9623;}i:421;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"  ";}i:2;i:9625;}i:422;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  =)   ";}i:2;i:9629;}i:423;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9638;}i:424;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-/";}i:2;i:9643;}i:425;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9646;}i:426;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-/  ";}i:2;i:9649;}i:427;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9658;}i:428;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-\";}i:2;i:9663;}i:429;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9666;}i:430;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-\  ";}i:2;i:9669;}i:431;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9678;}i:432;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-?";}i:2;i:9683;}i:433;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9686;}i:434;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-?  ";}i:2;i:9689;}i:435;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9698;}i:436;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-D";}i:2;i:9703;}i:437;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9706;}i:438;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-D  ";}i:2;i:9709;}i:439;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9718;}i:440;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-P";}i:2;i:9723;}i:441;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9726;}i:442;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-P  ";}i:2;i:9729;}i:443;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9738;}i:444;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-O";}i:2;i:9743;}i:445;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9746;}i:446;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-O  ";}i:2;i:9749;}i:447;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9758;}i:448;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-X";}i:2;i:9763;}i:449;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9766;}i:450;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-X  ";}i:2;i:9769;}i:451;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9778;}i:452;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":-|";}i:2;i:9783;}i:453;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9786;}i:454;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :-|  ";}i:2;i:9789;}i:455;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9798;}i:456;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:";-)";}i:2;i:9803;}i:457;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9806;}i:458;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  ;-)  ";}i:2;i:9809;}i:459;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9818;}i:460;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:"^_^";}i:2;i:9823;}i:461;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9826;}i:462;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  ^_^  ";}i:2;i:9829;}i:463;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9838;}i:464;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":?:";}i:2;i:9843;}i:465;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9846;}i:466;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :?:  ";}i:2;i:9849;}i:467;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9858;}i:468;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:":!:";}i:2;i:9863;}i:469;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9866;}i:470;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  :!:  ";}i:2;i:9869;}i:471;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9878;}i:472;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:3:"LOL";}i:2;i:9883;}i:473;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9886;}i:474;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:7:"  LOL  ";}i:2;i:9889;}i:475;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9898;}i:476;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:5:"FIXME";}i:2;i:9903;}i:477;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9908;}i:478;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:8:"  FIXME ";}i:2;i:9911;}i:479;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"
  * ";}i:2;i:9921;}i:480;a:3:{i:0;s:6:"smiley";i:1;a:1:{i:0;s:8:"DELETEME";}i:2;i:9926;}i:481;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:9934;}i:482;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:10:" DELETEME ";}i:2;i:9937;}i:483;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"

==== Text to ";}i:2;i:9949;}i:484;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:9964;}i:485;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:31:" Conversions ====

Typography: ";}i:2;i:9968;}i:486;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:8:"DokuWiki";i:1;N;}i:2;i:9999;}i:487;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:125:" can convert simple text characters to their typographically correct entities. Here is an example of recognized characters.

";}i:2;i:10011;}i:488;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"->";}i:2;i:10136;}i:489;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10138;}i:490;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"<-";}i:2;i:10139;}i:491;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10141;}i:492;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"<->";}i:2;i:10142;}i:493;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10145;}i:494;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"=>";}i:2;i:10146;}i:495;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10148;}i:496;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"<=";}i:2;i:10149;}i:497;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10151;}i:498;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"<=>";}i:2;i:10152;}i:499;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10155;}i:500;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:">>";}i:2;i:10156;}i:501;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10158;}i:502;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"<<";}i:2;i:10159;}i:503;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10161;}i:504;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:10162;}i:505;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10164;}i:506;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"---";}i:2;i:10165;}i:507;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10168;}i:508;a:3:{i:0;s:14:"multiplyentity";i:1;a:2:{i:0;s:3:"640";i:1;s:3:"480";}i:2;i:10169;}i:509;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10176;}i:510;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"(c)";}i:2;i:10177;}i:511;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10180;}i:512;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:4:"(tm)";}i:2;i:10181;}i:513;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10185;}i:514;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"(r)";}i:2;i:10186;}i:515;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"
";}i:2;i:10189;}i:516;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:10190;}i:517;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:31:"He thought 'It's a man's world'";}i:2;i:10191;}i:518;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"...";}i:2;i:10222;}i:519;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:10225;}i:520;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"

<code>
";}i:2;i:10226;}i:521;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"->";}i:2;i:10235;}i:522;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10237;}i:523;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"<-";}i:2;i:10238;}i:524;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10240;}i:525;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"<->";}i:2;i:10241;}i:526;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10244;}i:527;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"=>";}i:2;i:10245;}i:528;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10247;}i:529;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"<=";}i:2;i:10248;}i:530;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10250;}i:531;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"<=>";}i:2;i:10251;}i:532;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10254;}i:533;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:">>";}i:2;i:10255;}i:534;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10257;}i:535;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"<<";}i:2;i:10258;}i:536;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10260;}i:537;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:10261;}i:538;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10263;}i:539;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"---";}i:2;i:10264;}i:540;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10267;}i:541;a:3:{i:0;s:14:"multiplyentity";i:1;a:2:{i:0;s:3:"640";i:1;s:3:"480";}i:2;i:10268;}i:542;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10275;}i:543;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"(c)";}i:2;i:10276;}i:544;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10279;}i:545;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:4:"(tm)";}i:2;i:10280;}i:546;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:10284;}i:547;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"(r)";}i:2;i:10285;}i:548;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"
";}i:2;i:10288;}i:549;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:10289;}i:550;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:31:"He thought 'It's a man's world'";}i:2;i:10290;}i:551;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"...";}i:2;i:10321;}i:552;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:10324;}i:553;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:54:"
</code>

The same can be done to produce any kind of ";}i:2;i:10325;}i:554;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:10379;}i:555;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:", it just needs to be added to the ";}i:2;i:10383;}i:556;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:13:"doku>entities";i:1;s:12:"pattern file";i:2;s:4:"doku";i:3;s:8:"entities";}i:2;i:10418;}i:557;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:95:".

There are three exceptions which do not come from that pattern file: multiplication entity (";}i:2;i:10448;}i:558;a:3:{i:0;s:14:"multiplyentity";i:1;a:2:{i:0;s:3:"640";i:1;s:3:"480";}i:2;i:10543;}i:559;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:16:"), 'single' and ";}i:2;i:10550;}i:560;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:10566;}i:561;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:13:"double quotes";}i:2;i:10567;}i:562;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:10580;}i:563;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:". They can be turned off through a ";}i:2;i:10581;}i:564;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:22:"doku>config:typography";i:1;s:13:"config option";i:2;s:4:"doku";i:3;s:17:"config:typography";}i:2;i:10616;}i:565;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:156:".

===== Quoting =====

Some times you want to mark some text to show it's a reply or comment. You can use the following syntax:

  I think we should do it
";}i:2;i:10656;}i:566;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:19:"markdowku_linebreak";i:1;a:3:{i:0;s:3:"  
";i:1;i:5;i:2;i:10812;}i:2;i:5;i:3;s:3:"  
";}i:2;i:10812;}i:567;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:"  > No we shouldn't
";}i:2;i:10815;}i:568;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:19:"markdowku_linebreak";i:1;a:3:{i:0;s:3:"  
";i:1;i:5;i:2;i:10835;}i:2;i:5;i:3;s:3:"  
";}i:2;i:10835;}i:569;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"  ";}i:2;i:10838;}i:570;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:">>";}i:2;i:10840;}i:571;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:" Well, I say we should
";}i:2;i:10842;}i:572;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:19:"markdowku_linebreak";i:1;a:3:{i:0;s:3:"  
";i:1;i:5;i:2;i:10865;}i:2;i:5;i:3;s:3:"  
";}i:2;i:10865;}i:573;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"  > Really?
";}i:2;i:10868;}i:574;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:19:"markdowku_linebreak";i:1;a:3:{i:0;s:3:"  
";i:1;i:5;i:2;i:10880;}i:2;i:5;i:3;s:3:"  
";}i:2;i:10880;}i:575;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"  ";}i:2;i:10883;}i:576;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:">>";}i:2;i:10885;}i:577;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:" Yes!
";}i:2;i:10887;}i:578;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:19:"markdowku_linebreak";i:1;a:3:{i:0;s:3:"  
";i:1;i:5;i:2;i:10893;}i:2;i:5;i:3;s:3:"  
";}i:2;i:10893;}i:579;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:"  ";}i:2;i:10896;}i:580;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:">>";}i:2;i:10898;}i:581;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:64:"> Then lets do it!

I think we should do it

> No we shouldn't

";}i:2;i:10900;}i:582;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:">>";}i:2;i:10964;}i:583;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:" Well, I say we should

> Really?

";}i:2;i:10966;}i:584;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:">>";}i:2;i:11001;}i:585;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:" Yes!

";}i:2;i:11003;}i:586;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:">>";}i:2;i:11010;}i:587;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:366:"> Then lets do it!

===== Tables =====

DokuWiki supports a simple syntax to create tables.

^ Heading 1      ^ Heading 2       ^ Heading 3          ^
| Row 1 Col 1    | Row 1 Col 2     | Row 1 Col 3        |
| Row 2 Col 1    | some colspan (note the double pipe) ||
| Row 3 Col 1    | Row 3 Col 2     | Row 3 Col 3        |

Table rows have to start and end with a ";}i:2;i:11012;}i:588;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:11378;}i:589;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"|";}i:2;i:11380;}i:590;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:1:"|";}i:2;i:11380;}i:591;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:11381;}i:592;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:" for normal rows or a ";}i:2;i:11383;}i:593;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:11405;}i:594;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"^";}i:2;i:11407;}i:595;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:1:"^";}i:2;i:11407;}i:596;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:11408;}i:597;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1096:" for headers.

  ^ Heading 1      ^ Heading 2       ^ Heading 3          ^
  | Row 1 Col 1    | Row 1 Col 2     | Row 1 Col 3        |
  | Row 2 Col 1    | some colspan (note the double pipe) ||
  | Row 3 Col 1    | Row 3 Col 2     | Row 3 Col 3        |

To connect cells horizontally, just make the next cell completely empty as shown above. Be sure to have always the same amount of cell separators!

Vertical tableheaders are possible, too.

|              ^ Heading 1            ^ Heading 2          ^
^ Heading 3    | Row 1 Col 2          | Row 1 Col 3        |
^ Heading 4    | no colspan this time |                    |
^ Heading 5    | Row 2 Col 2          | Row 2 Col 3        |

As you can see, it's the cell separator before a cell which decides about the formatting:

  |              ^ Heading 1            ^ Heading 2          ^
  ^ Heading 3    | Row 1 Col 2          | Row 1 Col 3        |
  ^ Heading 4    | no colspan this time |                    |
  ^ Heading 5    | Row 2 Col 2          | Row 2 Col 3        |

You can have rowspans (vertically connected cells) by adding ";}i:2;i:11410;}i:598;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:12506;}i:599;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:":::";}i:2;i:12508;}i:600;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:3:":::";}i:2;i:12508;}i:601;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:12511;}i:602;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1547:" into the cells below the one to which they should connect.

^ Heading 1      ^ Heading 2                  ^ Heading 3          ^
| Row 1 Col 1    | this cell spans vertically | Row 1 Col 3        |
| Row 2 Col 1    | :::                        | Row 2 Col 3        |
| Row 3 Col 1    | :::                        | Row 2 Col 3        |

Apart from the rowspan syntax those cells should not contain anything else.

  ^ Heading 1      ^ Heading 2                  ^ Heading 3          ^
  | Row 1 Col 1    | this cell spans vertically | Row 1 Col 3        |
  | Row 2 Col 1    | :::                        | Row 2 Col 3        |
  | Row 3 Col 1    | :::                        | Row 2 Col 3        |

You can align the table contents, too. Just add at least two whitespaces at the opposite end of your text: Add two spaces on the left to align right, two spaces on the right to align left and two spaces at least at both ends for centered text.

^           Table with alignment           ^^^
|         right|    center    |left          |
|left          |         right|    center    |
| xxxxxxxxxxxx | xxxxxxxxxxxx | xxxxxxxxxxxx |

This is how it looks in the source:

  ^           Table with alignment           ^^^
  |         right|    center    |left          |
  |left          |         right|    center    |
  | xxxxxxxxxxxx | xxxxxxxxxxxx | xxxxxxxxxxxx |

Note: Vertical alignment is not supported.

===== No Formatting =====

If you need to display text exactly like it is typed (without any formatting), enclose the area either with ";}i:2;i:12513;}i:603;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:14060;}i:604;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"%%<nowiki>%%";}i:2;i:14062;}i:605;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:12:"%%<nowiki>%%";}i:2;i:14062;}i:606;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:14074;}i:607;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:49:" tags or even simpler, with double percent signs ";}i:2;i:14076;}i:608;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:14125;}i:609;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"<nowiki>%%</nowiki>";}i:2;i:14127;}i:610;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:19:"<nowiki>%%</nowiki>";}i:2;i:14127;}i:611;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:14146;}i:612;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:".

";}i:2;i:14148;}i:613;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:130:"
This is some text which contains addresses like this: http://www.splitbrain.org and **formatting**, but nothing is done with it.
";}i:2;i:14159;}i:614;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:"
The same is true for ";}i:2;i:14298;}i:615;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:35:"//__this__ text// with a smiley ;-)";}i:2;i:14322;}i:616;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:".

  ";}i:2;i:14359;}i:617;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:134:"
  This is some text which contains addresses like this: http://www.splitbrain.org and **formatting**, but nothing is done with it.
  ";}i:2;i:14372;}i:618;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:"
  The same is true for ";}i:2;i:14515;}i:619;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:35:"//__this__ text// with a smiley ;-)";}i:2;i:14541;}i:620;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:183:".

===== Code Blocks =====

You can include code blocks into your documents by either indenting them by at least two spaces (like used for the previous examples) or by using the tags ";}i:2;i:14578;}i:621;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:14761;}i:622;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"%%<code>%%";}i:2;i:14763;}i:623;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:10:"%%<code>%%";}i:2;i:14763;}i:624;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:14773;}i:625;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:" or ";}i:2;i:14775;}i:626;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:14779;}i:627;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"%%<file>%%";}i:2;i:14781;}i:628;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:10:"%%<file>%%";}i:2;i:14781;}i:629;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:14791;}i:630;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:123:".

  This is text is indented by two spaces.

<code>
This is preformatted code all spaces are preserved: like              ";}i:2;i:14793;}i:631;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"<-";}i:2;i:14916;}i:632;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:282:"this
</code>

<file>
This is pretty much the same, but you could use it to show that you quoted a file.
</file>

Those blocks were created by this source:

    This is text is indented by two spaces.

  <code>
  This is preformatted code all spaces are preserved: like              ";}i:2;i:14918;}i:633;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"<-";}i:2;i:15200;}i:634;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:152:"this
  </code>

  <file>
  This is pretty much the same, but you could use it to show that you quoted a file.
  </file>

==== Syntax Highlighting ====

";}i:2;i:15202;}i:635;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:13:"wiki:DokuWiki";i:1;N;}i:2;i:15354;}i:636;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:70:" can highlight sourcecode, which makes it easier to read. It uses the ";}i:2;i:15371;}i:637;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:28:"http://qbnz.com/highlighter/";i:1;s:5:"GeSHi";}i:2;i:15441;}i:638;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:28:" Generic Syntax Highlighter ";}i:2;i:15479;}i:639;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:15507;}i:640;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:228:" so any language supported by GeSHi is supported. The syntax uses the same code and file blocks described in the previous section, but this time the name of the language syntax to be highlighted is included inside the tag, e.g. ";}i:2;i:15509;}i:641;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:15737;}i:642;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:28:"<nowiki><code java></nowiki>";}i:2;i:15739;}i:643;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:28:"<nowiki><code java></nowiki>";}i:2;i:15739;}i:644;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:15767;}i:645;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:" or ";}i:2;i:15769;}i:646;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:15773;}i:647;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:28:"<nowiki><file java></nowiki>";}i:2;i:15775;}i:648;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:28:"<nowiki><file java></nowiki>";}i:2;i:15775;}i:649;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:15803;}i:650;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:16:".

<code java>
/";}i:2;i:15805;}i:651;a:3:{i:0;s:11:"strong_open";i:1;a:0:{}i:2;i:15821;}i:652;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:78:"
 * The HelloWorldApp class implements an application that
 * simply displays ";}i:2;i:15823;}i:653;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:15901;}i:654;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"Hello World!";}i:2;i:15902;}i:655;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:15914;}i:656;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:123:" to the standard output.
 */
class HelloWorldApp {
    public static void main(String[] args) {
        System.out.println(";}i:2;i:15915;}i:657;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:16038;}i:658;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"Hello World!";}i:2;i:16039;}i:659;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:16051;}i:660;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"); ";}i:2;i:16052;}i:661;a:3:{i:0;s:13:"emphasis_open";i:1;a:0:{}i:2;i:16055;}i:662;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:94:"Display the string.
    }
}
</code>

The following language strings are currently recognized: ";}i:2;i:16057;}i:663;a:3:{i:0;s:14:"emphasis_close";i:1;a:0:{}i:2;i:16151;}i:664;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:262:"4cs, 6502acme, 6502kickass, 6502tasm, 68000devpac, abap, actionscript-french, actionscript, actionscript3, ada, algol68, apache, applescript, asm, asp, autoconf, autohotkey, autoit, avisynth, awk, bascomavr, bash, basic4gl, bf, bibtex, blitzbasic, bnf, boo, c, c";}i:2;i:16153;}i:665;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:25:"markdowku_italicunderline";i:1;a:2:{i:0;i:1;i:1;s:1:"_";}i:2;i:1;i:3;s:1:"_";}i:2;i:16415;}i:666;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:25:"markdowku_italicunderline";i:1;a:2:{i:0;i:3;i:1;s:13:"loadrunner, c";}i:2;i:3;i:3;s:13:"loadrunner, c";}i:2;i:16416;}i:667;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:25:"markdowku_italicunderline";i:1;a:2:{i:0;i:4;i:1;s:1:"_";}i:2;i:4;i:3;s:1:"_";}i:2;i:16429;}i:668;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1316:"mac, caddcl, cadlisp, cfdg, cfm, chaiscript, cil, clojure, cmake, cobol, coffeescript, cpp, cpp-qt, csharp, css, cuesheet, d, dcs, delphi, diff, div, dos, dot, e, epc, ecmascript, eiffel, email, erlang, euphoria, f1, falcon, fo, fortran, freebasic, fsharp, gambas, genero, genie, gdb, glsl, gml, gnuplot, go, groovy, gettext, gwbasic, haskell, hicest, hq9plus, html, html5, icon, idl, ini, inno, intercal, io, j, java5, java, javascript, jquery, kixtart, klonec, klonecpp, latex, lb, lisp, llvm, locobasic, logtalk, lolcode, lotusformulas, lotusscript, lscript, lsl2, lua, m68k, magiksf, make, mapbasic, matlab, mirc, modula2, modula3, mmix, mpasm, mxml, mysql, newlisp, nsis, oberon2, objc, objeck, ocaml-brief, ocaml, oobas, oracle8, oracle11, oxygene, oz, pascal, pcre, perl, perl6, per, pf, php-brief, php, pike, pic16, pixelbender, pli, plsql, postgresql, povray, powerbuilder, powershell, proftpd, progress, prolog, properties, providex, purebasic, pycon, python, q, qbasic, rails, rebol, reg, robots, rpmspec, rsplus, ruby, sas, scala, scheme, scilab, sdlbasic, smalltalk, smarty, sql, systemverilog, tcl, teraterm, text, thinbasic, tsql, typoscript, unicon, uscript, vala, vbnet, vb, verilog, vhdl, vim, visualfoxpro, visualprolog, whitespace, winbatch, whois, xbasic, xml, xorg_conf, xpp, yaml, z80, zxbasic";}i:2;i:16430;}i:669;a:3:{i:0;s:13:"emphasis_open";i:1;a:0:{}i:2;i:17746;}i:670;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:55:"

==== Downloadable Code Blocks ====

When you use the ";}i:2;i:17748;}i:671;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:17803;}i:672;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:6:"<code>";}i:2;i:17807;}i:673;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:17815;}i:674;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:" or ";}i:2;i:17817;}i:675;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:17821;}i:676;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:6:"<file>";}i:2;i:17825;}i:677;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:17833;}i:678;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:205:" syntax as above, you might want to make the shown code available for download as well. You can do this by specifying a file name after language code like this:

<code>
<file php myexample.php>
<?php echo ";}i:2;i:17835;}i:679;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:18040;}i:680;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"hello world!";}i:2;i:18041;}i:681;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:18053;}i:682;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:58:"; ?>
</file>
</code>

<file php myexample.php>
<?php echo ";}i:2;i:18054;}i:683;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:18112;}i:684;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"hello world!";}i:2;i:18113;}i:685;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:18125;}i:686;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:95:"; ?>
</file>

If you don't want any highlighting but want a downloadable file, specify a dash (";}i:2;i:18126;}i:687;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:18221;}i:688;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"-";}i:2;i:18223;}i:689;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:18224;}i:690;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:") as the language code: ";}i:2;i:18226;}i:691;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:18250;}i:692;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:19:"<code - myfile.foo>";}i:2;i:18254;}i:693;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:18275;}i:694;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:20:".


===== Embedding ";}i:2;i:18277;}i:695;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18297;}i:696;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:34:" and PHP =====

You can embed raw ";}i:2;i:18301;}i:697;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18335;}i:698;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:46:" or PHP code into your documents by using the ";}i:2;i:18339;}i:699;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:18385;}i:700;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:6:"<html>";}i:2;i:18389;}i:701;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:18397;}i:702;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:" or ";}i:2;i:18399;}i:703;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:18403;}i:704;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:5:"<php>";}i:2;i:18407;}i:705;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:18414;}i:706;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:74:" tags. (Use uppercase tags if you need to enclose block level elements.)

";}i:2;i:18416;}i:707;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18490;}i:708;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:50:" example:

<code>
<html>
This is some <span style=";}i:2;i:18494;}i:709;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:18544;}i:710;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:25:"color:red;font-size:150%;";}i:2;i:18545;}i:711;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:18570;}i:712;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:">inline ";}i:2;i:18571;}i:713;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18579;}i:714;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"</span>
</html>
<";}i:2;i:18583;}i:715;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18600;}i:716;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:">
<p style=";}i:2;i:18604;}i:717;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:18615;}i:718;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:"border:2px dashed red;";}i:2;i:18616;}i:719;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:18638;}i:720;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:">And this is some block ";}i:2;i:18639;}i:721;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18663;}i:722;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"</p>
</";}i:2;i:18667;}i:723;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18674;}i:724;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:43:">
</code>

<html>
This is some <span style=";}i:2;i:18678;}i:725;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:18721;}i:726;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:25:"color:red;font-size:150%;";}i:2;i:18722;}i:727;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:18747;}i:728;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:">inline ";}i:2;i:18748;}i:729;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18756;}i:730;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"</span>
</html>
<";}i:2;i:18760;}i:731;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18777;}i:732;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:">
<p style=";}i:2;i:18781;}i:733;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:18792;}i:734;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:"border:2px dashed red;";}i:2;i:18793;}i:735;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:18815;}i:736;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:">And this is some block ";}i:2;i:18816;}i:737;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18840;}i:738;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"</p>
</";}i:2;i:18844;}i:739;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18851;}i:740;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:100:">

PHP example:

<code>
<php>
echo 'The PHP version: ';
echo phpversion();
echo ' (generated inline ";}i:2;i:18855;}i:741;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:18955;}i:742;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:36:")';
</php>
<PHP>
echo '<table class=";}i:2;i:18959;}i:743;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:18995;}i:744;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"inline";}i:2;i:18996;}i:745;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:19002;}i:746;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:198:"><tr><td>The same, but inside a block level element:</td>';
echo '<td>'.phpversion().'</td>';
echo '</tr></table>';
</PHP>
</code>

<php>
echo 'The PHP version: ';
echo phpversion();
echo ' (inline ";}i:2;i:19003;}i:747;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:19201;}i:748;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:36:")';
</php>
<PHP>
echo '<table class=";}i:2;i:19205;}i:749;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:19241;}i:750;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"inline";}i:2;i:19242;}i:751;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:19248;}i:752;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:124:"><tr><td>The same, but inside a block level element:</td>';
echo '<td>'.phpversion().'</td>';
echo '</tr></table>';
</PHP>

";}i:2;i:19249;}i:753;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:1;i:1;s:2:"**";}i:2;i:1;i:3;s:2:"**";}i:2;i:19373;}i:754;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:3;i:1;s:11:"Please Note";}i:2;i:3;i:3;s:11:"Please Note";}i:2;i:19375;}i:755;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:4;i:1;s:2:"**";}i:2;i:4;i:3;s:2:"**";}i:2;i:19386;}i:756;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:2:": ";}i:2;i:19388;}i:757;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"HTML";}i:2;i:19390;}i:758;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:159:" and PHP embedding is disabled by default in the configuration. If disabled, the code is displayed instead of executed.

===== RSS/ATOM Feed Aggregation =====
";}i:2;i:19394;}i:759;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:8:"DokuWiki";i:1;N;}i:2;i:19553;}i:760;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:72:" can integrate data from external XML feeds. For parsing the XML feeds, ";}i:2;i:19565;}i:761;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:21:"http://simplepie.org/";i:1;s:9:"SimplePie";}i:2;i:19637;}i:762;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:446:" is used. All formats understood by SimplePie can be used in DokuWiki as well. You can influence the rendering by multiple additional space separated parameters:

^ Parameter  ^ Description ^
| any number | will be used as maximum number items to show, defaults to 8 |
| reverse    | display the last items in the feed first |
| author     | show item authors names |
| date       | show item dates |
| description| show the item description. If ";}i:2;i:19672;}i:763;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:18:"doku>config:htmlok";i:1;s:4:"HTML";i:2;s:4:"doku";i:3;s:13:"config:htmlok";}i:2;i:20118;}i:764;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:94:" is disabled all tags will be stripped |
| nosort     | do not sort the items in the feed |
| ";}i:2;i:20145;}i:765;a:3:{i:0;s:14:"emphasis_close";i:1;a:0:{}i:2;i:20239;}i:766;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:"n";}i:2;i:20241;}i:767;a:3:{i:0;s:13:"emphasis_open";i:1;a:0:{}i:2;i:20242;}i:768;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:182:"[dhm] | refresh period, where d=days, h=hours, m=minutes. (e.g. 12h = 12 hours). |

The refresh period defaults to 4 hours. Any value below 10 minutes will be treated as 10 minutes. ";}i:2;i:20244;}i:769;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:13:"wiki:DokuWiki";i:1;N;}i:2;i:20426;}i:770;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:159:" will generally try to supply a cached version of a page, obviously this is inappropriate when the page contains dynamic external content. The parameter tells ";}i:2;i:20443;}i:771;a:3:{i:0;s:12:"internallink";i:1;a:2:{i:0;s:13:"wiki:DokuWiki";i:1;N;}i:2;i:20602;}i:772;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:42:" to re-render the page if it is more than ";}i:2;i:20619;}i:773;a:3:{i:0;s:14:"emphasis_close";i:1;a:0:{}i:2;i:20661;}i:774;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"refresh period";}i:2;i:20663;}i:775;a:3:{i:0;s:13:"emphasis_open";i:1;a:0:{}i:2;i:20677;}i:776;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:142:" since the page was last rendered.

By default the feed will be sorted by date, newest items first. You can sort it by oldest first using the ";}i:2;i:20679;}i:777;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:20821;}i:778;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"reverse";}i:2;i:20823;}i:779;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:20830;}i:780;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:43:" parameter, or display the feed as is with ";}i:2;i:20832;}i:781;a:3:{i:0;s:14:"monospace_open";i:1;a:0:{}i:2;i:20875;}i:782;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"nosort";}i:2;i:20877;}i:783;a:3:{i:0;s:15:"monospace_close";i:1;a:0:{}i:2;i:20883;}i:784;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:".

";}i:2;i:20885;}i:785;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:1;i:1;s:2:"**";}i:2;i:1;i:3;s:2:"**";}i:2;i:20888;}i:786;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:3;i:1;s:8:"Example:";}i:2;i:3;i:3;s:8:"Example:";}i:2;i:20890;}i:787;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:22:"markdowku_boldasterisk";i:1;a:2:{i:0;i:4;i:1;s:2:"**";}i:2;i:4;i:3;s:2:"**";}i:2;i:20898;}i:788;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"

  ";}i:2;i:20900;}i:789;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:20904;}i:790;a:3:{i:0;s:3:"rss";i:1;a:2:{i:0;s:29:"http://slashdot.org/index.rss";i:1;a:6:{s:3:"max";s:1:"5";s:7:"reverse";i:0;s:6:"author";i:1;s:4:"date";i:1;s:7:"details";i:0;s:7:"refresh";i:3600;}}i:2;i:20904;}i:791;a:3:{i:0;s:3:"rss";i:1;a:2:{i:0;s:29:"http://slashdot.org/index.rss";i:1;a:6:{s:3:"max";s:1:"5";s:7:"reverse";i:0;s:6:"author";i:1;s:4:"date";i:1;s:7:"details";i:0;s:7:"refresh";i:3600;}}i:2;i:20961;}i:792;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20961;}i:793;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:199:"


===== Control Macros =====

Some syntax influences how DokuWiki renders a page without creating any output it self. The following control macros are availble:

^ Macro           ^ Description |
| ";}i:2;i:21016;}i:794;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:9:"~~NOTOC~~";}i:2;i:21217;}i:795;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:82:"   | If this macro is found on the page, no table of contents will be created |
| ";}i:2;i:21228;}i:796;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:11:"~~NOCACHE~~";}i:2;i:21312;}i:797;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:91:" | DokuWiki caches all output by default. Sometimes this might not be wanted (eg. when the ";}i:2;i:21325;}i:798;a:3:{i:0;s:11:"unformatted";i:1;a:1:{i:0;s:5:"<php>";}i:2;i:21418;}i:799;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:163:" syntax above is used), adding this macro will force DokuWiki to rerender a page on every call |

===== Syntax Plugins =====

DokuWiki's syntax can be extended by ";}i:2;i:21425;}i:800;a:3:{i:0;s:13:"interwikilink";i:1;a:4:{i:0;s:12:"doku>plugins";i:1;s:7:"Plugins";i:2;s:4:"doku";i:3;s:7:"plugins";}i:2;i:21588;}i:801;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:174:". How the installed plugins are used is described on their appropriate description pages. The following syntax plugins are available in this particular DokuWiki installation:";}i:2;i:21612;}i:802;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:21788;}i:803;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:4:"info";i:1;a:1:{i:0;s:13:"syntaxplugins";}i:2;i:5;i:3;s:22:"~~INFO:syntaxplugins~~";}i:2;i:21788;}i:804;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:21810;}i:805;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:21810;}i:806;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:21810;}i:807;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:21810;}i:808;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:21810;}}
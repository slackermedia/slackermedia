a:96:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:26:"Chapter 1.  GIMP Brushes";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:27;}i:4;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:27;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"Table of Contents";}i:2;i:29;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:46;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:46;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"Gimp Paint Studio";}i:2;i:48;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:65;}i:10;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:65;}i:11;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"See Also";}i:2;i:67;}i:12;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:75;}i:13;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:75;}i:14;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:22:"MyPaint
Krita
Inkscape";}i:2;i:77;}i:15;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:99;}i:16;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:99;}i:17;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:198:"Ever since version 2.4, GIMP can use Photoshop ABR brushes, so almost any
website offering free brushes for image manipulation programs will have
something to offer an artist in need of new brushes.";}i:2;i:101;}i:18;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:299;}i:19;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:299;}i:20;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:447:"These sites seem to come and go at random, but the Great Linux Multimedia
Sprints has plenty for you to freely install, use, and re-distribute. Be aware
that not all brush sets are create equal; some are fine at low resolutions but
degrade when increased in size, so audition the brushes that you install and
judge them based on your own requirements. Installing too many will cause the
brush menu in GIMP to respond slower, so don't go overboard.";}i:2;i:301;}i:21;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:748;}i:22;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:748;}i:23;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:34:"Installing GIMP brushes is simple:";}i:2;i:750;}i:24;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:784;}i:25;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:784;}i:26;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:784;}i:27;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:784;}i:28;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:1;i:3;s:6:"

  * ";}i:2;i:784;}i:29;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:70:"To install brushes on a system-wide basis, move the brush files (*.gbr";}i:2;i:790;}i:30;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:70:"To install brushes on a system-wide basis, move the brush files (*.gbr";}i:2;i:790;}i:31;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:860;}i:32;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:860;}i:33;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:860;}i:34;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:61:".arb *.vbr etc) to the /usr/share/gimp/2.0/brushes/ directory";}i:2;i:866;}i:35;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:927;}i:36;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:927;}i:37;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:927;}i:38;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:928;}i:39;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:928;}i:40;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:928;}i:41;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:928;}i:42;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:2;i:3;s:5:"
  * ";}i:2;i:928;}i:43;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:68:"To install them on a per-user basis, copy the files to that user's ~";}i:2;i:933;}i:44;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:68:"To install them on a per-user basis, copy the files to that user's ~";}i:2;i:933;}i:45;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:33:"
    /.gimp-2.x/brushes directory";}i:2;i:1001;}i:46;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:32:"    /.gimp-2.x/brushes directory";}i:2;i:1002;}i:47;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:1034;}i:48;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:1034;}i:49;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:1034;}i:50;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:1034;}i:51;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1034;}i:52;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:470:"Launch GIMP (or quit and then launch, if it was already running) and the
brushes will in the brush palette. Notice that GIMP scans the brushes directory
every time it launches, so keeping hundreds or brushes in the the brushes
directory will make GIMP slower to launch. It may behoove you to familiarize
yourself with the brushes you own, and only keep your favourite brushes in the
brush directory, until GIMP developers come up with a more dynamic way to load
brushes.";}i:2;i:1036;}i:53;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1506;}i:54;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1506;}i:55;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"Gimp Paint Studio";}i:2;i:1508;}i:56;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1525;}i:57;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1525;}i:58;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"GIMP Paint Studio (";}i:2;i:1527;}i:59;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:1546;}i:60;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"GPS";}i:2;i:1547;}i:61;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:1550;}i:62;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:323:" for short) is a sort of project-within-a-project; it
is an add-on pack for GIMP itself and brings with it a whole new personality by
adding sets of brushes, color swatches, and presets so that you can achieve
everything from realistic materials and brush emulation, airbrushing, inking,
and any variety of special effects.";}i:2;i:1551;}i:63;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1874;}i:64;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1874;}i:65;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:204:"GPS is available from code.google.com/p/gps-gimp-paint-studio as a zip file. It
releases as a traditional software would, so make sure to download the latest
version corresponding to your version of GIMP.";}i:2;i:1876;}i:66;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2080;}i:67;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2080;}i:68;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:92:"Slackermedia's maintainers currently also maintains a SlackBuild for GPS on
slackbuilds.org.";}i:2;i:2082;}i:69;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2174;}i:70;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2174;}i:71;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:135:"If you choose to manually install it, the install is simple; unzip the file and
then copy the brushes into your GIMP brushes directory.";}i:2;i:2176;}i:72;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2311;}i:73;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2311;}i:74;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"Warning";}i:2;i:2313;}i:75;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2320;}i:76;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2320;}i:77;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:125:"If you have custom GIMP *rc files, GPS will overwrite them during installation.
Backup your GIMP *rc files before proceeding.";}i:2;i:2322;}i:78;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2447;}i:79;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:307:"$

   curl
   http://gps-gimp-paint-studio.googlecode.com/files/GPS%201_5_final%20release.zip
   -O GPS1_5_finalrelease.zip; mkdir GPS_source

$

  unzip GPS*zip -d GPS_source ; cd ./GPS_source

$

  for gps in $(find ./* -type d) ; do cp $gps/*
  ~/.gimp*/$gps ; done

$

    cp ./GPS_source/*rc ~/.gimp*/
";}i:2;i:2447;}i:80;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:2832;}i:81;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2832;}i:82;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:80:"Once the files are copied, launch or re-launch GIMP and explore the new
options.";}i:2;i:2835;}i:83;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2915;}i:84;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2915;}i:85;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"Warning";}i:2;i:2917;}i:86;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2924;}i:87;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2924;}i:88;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:80:"GPS brushes are only brush presets, so they are not stored in your brushes
menu.";}i:2;i:2926;}i:89;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3006;}i:90;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3006;}i:91;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:54:"The new brushes are stored in the Restore Options From";}i:2;i:3008;}i:92;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"...";}i:2;i:3062;}i:93;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:108:" button at the bottom
of the Paint Brush tab. Everything else is easily accessible in their
respective tabs.";}i:2;i:3065;}i:94;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3173;}i:95;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:3173;}}
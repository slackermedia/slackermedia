a:460:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"Chapter 1. ffmpeg";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:20;}i:4;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"Table of Contents";}i:2;i:22;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:39;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:39;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:60:"Slackermedia's Kitchen-Sink Build
ffmpeg2theora
FFmpeg HOWTO";}i:2;i:41;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:101;}i:10;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:191:"Why Compress Video?
Know Your Video
Codecs and Containers
Frame Size
Bit Rate
I,B,P Frames
Variable and Constant Bit Rates
2-pass Encoding
Frame Rate
Audio
Threads
How to Test Before Encoding";}i:2;i:101;}i:11;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:342;}i:12;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:342;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Strengths";}i:2;i:344;}i:14;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:353;}i:15;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:353;}i:16;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:18:"Codec Indifference";}i:2;i:355;}i:17;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:373;}i:18;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:288:"Ffmpeg contains one of the most powerful libraries of multimedia (on any
platform, evident by how common it is used in both open and closed source
applications). A complete build of ffmpeg can ingest, process, and output
nearly every video, audio, and image in common use (and then some).";}i:2;i:373;}i:19;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:679;}i:20;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:679;}i:21;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Scripting";}i:2;i:681;}i:22;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:690;}i:23;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:107:"Ffmpeg is a shell command as well as a set of libraries, so it can be
invoked directly or as a sub-process.";}i:2;i:690;}i:24;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:807;}i:25;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:807;}i:26;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"Multi-threaded";}i:2;i:809;}i:27;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:823;}i:28;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:91:"Encoding media is famously intensive, so why not use every core your
computer has to offer?";}i:2;i:823;}i:29;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:924;}i:30;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:924;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"Weaknesses";}i:2;i:926;}i:32;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:936;}i:33;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:936;}i:34;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"Complex";}i:2;i:938;}i:35;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:945;}i:36;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:322:"This is an application filled with features, and it deals with the tangled
mess that is media codecs as well as anything can. It does take time to
learn it, and then more time to understand how all of the features of
encoding actually work. This is no worse than any other option; media
encoding is a difficult enterprise.";}i:2;i:945;}i:37;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:1289;}i:38;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1289;}i:39;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:13:"Moving Target";}i:2;i:1291;}i:40;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1304;}i:41;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:174:"Compatibility with media codecs is a moving target, so ffmpeg is subject to
sometimes drastic changes on a fairly regular basis. Upgrading too often is
generally not advised.";}i:2;i:1304;}i:42;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:1492;}i:43;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1492;}i:44;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"See Also";}i:2;i:1494;}i:45;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1502;}i:46;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1502;}i:47;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:17:"Gstreamer
Mplayer";}i:2;i:1504;}i:48;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1521;}i:49;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1521;}i:50;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:539:"ffmpeg is the backbone of many video-related applications from players to
editors (and is in fact even both of these things in itself). For the most
flexible video and audio system possible, ffmpeg should be installed directly
from the latest source code with as many codec options turned on as possible.
Find the code at ffmpeg.org. It's a big application with many options and you
will want to have control over whether these options are enabled or disabled,
so edit the SlackBuild script to explicitly activate all the options you need.";}i:2;i:1523;}i:51;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2062;}i:52;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2062;}i:53;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Important";}i:2;i:2064;}i:54;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2073;}i:55;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2073;}i:56;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:570:"ffmpeg has been forked into a project called libav, which provides a series of
libav libraries and a shell application avconv. To confuse matters, some Linux
distributions aliased the ffmpeg command to the avconv binary (but not the
libraries), leading to some confusion about how the two projects relate to one
another and whether they are inter-changeable or not. In practise, the two have
been nearly 100%compatible, but there are reports that there are applications
that do not work with libav, and as the projects evolve they are almost sure to
diverge in features.";}i:2;i:2075;}i:57;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2645;}i:58;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2645;}i:59;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:350:"After having run both ffmpeg and libav since the previous edition of this
manual, it is the maintainer or Slackermedia's recommendation that you use 
ffmpeg for maximum assured compatibility. (You are free to have both installed,
but there is duplication of function, so while it is safe, there has been no
indication so far that it is advantageous.)";}i:2;i:2647;}i:60;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2997;}i:61;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2997;}i:62;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:137:"To learn about the compiling options, read the ffmpeg compiling documentation
found in the source code tarball, and then try ./configure ";}i:2;i:2999;}i:63;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:2:"--";}i:2;i:3136;}i:64;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"help";}i:2;i:3138;}i:65;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3142;}i:66;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3142;}i:67;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:297:"Since a SlackBuild is just a shell script, you can specify variables from the
command line that will be passed to the SlackBuild script, although this tends
to be just as complex as editing the SlackBuild script itself. The results are
the same, so use whatever method is most comfortable for you:";}i:2;i:3144;}i:68;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3442;}i:69;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:36:"VPX=yes XVID=yes DIRAC=yes AND SO ON";i:1;i:1;i:2;i:3442;}i:2;i:3442;}i:70;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:3442;}i:71;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:19:"markdowku_headeratx";i:1;b:1;i:2;i:5;i:3;s:41:"
#  VPX=yes XVID=yes DIRAC=yes AND SO ON ";}i:2;i:3442;}i:72;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3442;}i:73;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"./ffmpeg.SlackBuild";}i:2;i:3484;}i:74;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3503;}i:75;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3503;}i:76;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:33:"Slackermedia's Kitchen-Sink Build";}i:2;i:3505;}i:77;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3538;}i:78;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3538;}i:79;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:255:"Building ffmpeg with all of its options activated can take a lot of time and
involve a lot of hunting for some pretty obscure codec libraries. You are
welcome to download the installable ffmpeg and ffmpeg-related packages from the
Slackermedia website at ";}i:2;i:3540;}i:80;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:36:"http://slackermedia.info/slackbuilds";i:1;N;}i:2;i:3795;}i:81;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:3831;}i:82;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3832;}i:83;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3832;}i:84;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:13:"ffmpeg2theora";}i:2;i:3834;}i:85;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3847;}i:86;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3847;}i:87;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:173:"ffmpeg2theora is not a part of <zapp>ffmpeg</zapp> but an independent frontend
for converting any format that ffmpeg can read into the free Ogg Vorbis and Ogg
Theora codecs.";}i:2;i:3849;}i:88;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4022;}i:89;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4022;}i:90;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:215:"There may be no real advantage to using ffmpeg2theora rather than just using 
ffmpeg, but it does have an abbreviated set of options since its goal is far
more focused than the near-infinite possibilities of ffmpeg.";}i:2;i:4024;}i:91;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4239;}i:92;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4239;}i:93;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"Tip";}i:2;i:4241;}i:94;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4244;}i:95;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4244;}i:96;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:246:"For some video (especially high-res video meant to be dynamically scaled on any
variety of device with any variety of screen size), you should also consider
the webm format, which is also free and open source like Theora, directly
through ffmpeg.";}i:2;i:4246;}i:97;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4492;}i:98;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4492;}i:99;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:126:"The command structure is similar, so if you've used ffmpeg or mencoder to
transcode media then it will all feel very familiar:";}i:2;i:4494;}i:100;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4620;}i:101;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4620;}i:102;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:98:"$ ffmpeg2theora input_file.avi -x 1920 -y
1080 -V 21000kbps -A 320kbps -c 2 -H 44100 -o output.ogv";}i:2;i:4622;}i:103;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4720;}i:104;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4720;}i:105;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"In other words:";}i:2;i:4723;}i:106;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4739;}i:107;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:218:"$ ffmpeg2theora [input filename] -x
[target horizontal pixel count] -y [target vertical
pixel count] -V [target bitrate in kbps] -A [audio
bit rate] -c [audio channels] -H [audio sample
rate in Hz] -o [output filename]";}i:2;i:4739;}i:108;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4739;}i:109;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:88:"ffmpeg2theora is available from slackbuilds.org and requires no special compile
options.";}i:2;i:4971;}i:110;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5059;}i:111;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5059;}i:112;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"FFmpeg HOWTO";}i:2;i:5061;}i:113;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5073;}i:114;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5073;}i:115;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:332:"Unlike proprietary video converters, ffmpeg is versatile; since it has no
commercial interests, it will decode and encode any format aside from those
that proprietary vendors successfully obfuscate. ffmpeg is primarily a command
line application, so controlling it is direct, simple, and can be scripted to
handle repetitious tasks.";}i:2;i:5075;}i:116;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5407;}i:117;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5407;}i:118;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:322:"Understanding how video compression and ffmpeg work doesn't entirely remove the
element of chance from video encoding, but it does help you understand how to
begin an attempt at transcoding or compressing. In a worst case scenario, it
can help you determine what went wrong with a badly compressed video and how to
fix it.";}i:2;i:5409;}i:119;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5731;}i:120;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5731;}i:121;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:19:"Why Compress Video?";}i:2;i:5733;}i:122;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5752;}i:123;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5752;}i:124;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:121:"Nobody compresses or transcodes their video because they want to. We process
video through ffmpeg for one of two reasons:";}i:2;i:5754;}i:125;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5875;}i:126;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:5875;}i:127;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:5875;}i:128;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:5875;}i:129;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:1;i:3;s:6:"

  * ";}i:2;i:5875;}i:130;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:70:"The file size is too large for convenient storage (on a hard drive) or";}i:2;i:5881;}i:131;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:70:"The file size is too large for convenient storage (on a hard drive) or";}i:2;i:5881;}i:132;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:35:"
    transmission (over a network).";}i:2;i:5951;}i:133;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:34:"    transmission (over a network).";}i:2;i:5952;}i:134;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5986;}i:135;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:79:"    In ths case, you're intentionally compressing video in order to reduce file";}i:2;i:5988;}i:136;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:79:"    In ths case, you're intentionally compressing video in order to reduce file";}i:2;i:5988;}i:137;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:68:"
    size. You're taking a loss in quality in favour of convenience.";}i:2;i:6067;}i:138;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:67:"    size. You're taking a loss in quality in favour of convenience.";}i:2;i:6068;}i:139;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:6135;}i:140;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6136;}i:141;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:6136;}i:142;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:6136;}i:143;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:6136;}i:144;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:6136;}i:145;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:2;i:3;s:5:"
  * ";}i:2;i:6136;}i:146;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:70:"A file's video format cannot be played (or cannot be played well) on a";}i:2;i:6141;}i:147;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:70:"A file's video format cannot be played (or cannot be played well) on a";}i:2;i:6141;}i:148;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:79:"
    device (like a media player) or used in a program (like a video editor, or";}i:2;i:6211;}i:149;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:78:"    device (like a media player) or used in a program (like a video editor, or";}i:2;i:6212;}i:150;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:45:"
    a streaming server) that we want to use.";}i:2;i:6290;}i:151;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:44:"    a streaming server) that we want to use.";}i:2;i:6291;}i:152;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:6335;}i:153;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:74:"    In this scenario, you are exclusively concerned with compatibility and";}i:2;i:6337;}i:154;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:74:"    In this scenario, you are exclusively concerned with compatibility and";}i:2;i:6337;}i:155;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:77:"
    would rather not take any loss in quality. Ideally, transcoding from one";}i:2;i:6411;}i:156;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:76:"    would rather not take any loss in quality. Ideally, transcoding from one";}i:2;i:6412;}i:157;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:75:"
    format to another would provide a file with the exact same quality; it";}i:2;i:6488;}i:158;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:74:"    format to another would provide a file with the exact same quality; it";}i:2;i:6489;}i:159;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:79:"
    would be a 1:1 process. In fact, however, all pixels are being re-analyzed";}i:2;i:6563;}i:160;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:78:"    would be a 1:1 process. In fact, however, all pixels are being re-analyzed";}i:2;i:6564;}i:161;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:75:"
    and re-written, so it's best to think of it as compression even though";}i:2;i:6642;}i:162;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:74:"    and re-written, so it's best to think of it as compression even though";}i:2;i:6643;}i:163;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:64:"
    compression is precisely what you might be trying to avoid.";}i:2;i:6717;}i:164;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:3;i:3;s:63:"    compression is precisely what you might be trying to avoid.";}i:2;i:6718;}i:165;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6781;}i:166;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:6781;}i:167;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:6781;}i:168;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:6781;}i:169;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:16:"markdowku_ulists";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:6781;}i:170;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:6781;}i:171;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"Know Your Video";}i:2;i:6783;}i:172;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6798;}i:173;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:6798;}i:174;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:275:"Before making an attempt to convert your video to some other format or size,
you should first look at the video's current attributes so that you can make
intelligent choices about how you convert. To see all the most important
information about a video file, use the command:";}i:2;i:6800;}i:175;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:7076;}i:176;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:24:"$  ffmpeg -i myVideo.mts";}i:2;i:7076;}i:177;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:7076;}i:178;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:289:"The output of this command can be pretty messy (especially if you're not
entirely sure what you're looking for), so if you prefer to use a front end
that parses the output then install and use either ??? or ??? or even vlc, all
of which provide the same information in easy-to-read output.";}i:2;i:7105;}i:179;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:7394;}i:180;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:7394;}i:181;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:21:"Codecs and Containers";}i:2;i:7396;}i:182;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:7417;}i:183;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:7417;}i:184;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:475:"Which video and audio codec you choose for your video is often dictated to you.
If you're encoding so that you can play a video on a specific media player,
gadget, or software that will only play h.264, for instance, then your choice
is clear. On the other hand, if you're encoding for HTML5 then your choices are
Theora, Webm, or h.264. If it's for personal use or for some software that is
flexible in the codecs it can work with, then your choices may be nearly
limitless.";}i:2;i:7419;}i:185;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:7894;}i:186;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:7894;}i:187;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:89:"Your video probably also contains audio, so you'll need to choose an audio
codec as well.";}i:2;i:7896;}i:188;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:7985;}i:189;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:7985;}i:190;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:86:"To complicate matters, your video also must be placed into a specific file
format, or ";}i:2;i:7987;}i:191;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:8073;}i:192;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"container";}i:2;i:8074;}i:193;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:8083;}i:194;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:428:". While some container formats are very flexible and
allow you to put nearly any combination of video, audio, frame size, aspect
ratio, and frame rate into it, others are very strict. The .mpg container, for
instance, specifically holds MPEG video (and then only layers 1 or 2), and
that's all it can do. The .mkv container, on the other hand, is designed to be
as flexible as possible and holds nearly anything you put into it.";}i:2;i:8084;}i:195;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:8512;}i:196;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:8512;}i:197;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:562:"If you are encoding for you own use, then you are free to use any container
format you please; as long as your media software is happy with it, then
anything is fair game. However, if you expect your media to be playable on
devices or software that you do not control then you need to choose carefully
by reading up on what is supported by the device or software you are targeting.
Ffmpeg will not always prevent you from creating files that are impossible to
play back, and some players will even play containers that contain incorrect
codecs (think of it as a ";}i:2;i:8514;}i:198;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:9076;}i:199;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"quirks-mode";}i:2;i:9077;}i:200;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:9088;}i:201;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:124:" for video players), so if you're
experimenting with new file formats then look them up to make sure you stay
safely within ";}i:2;i:9089;}i:202;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"spec";}i:2;i:9213;}i:203;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:9217;}i:204;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:9218;}i:205;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:9218;}i:206;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:718:"It sounds confusing, and it is, but the good news is that (broadly speaking),
all video codecs do basically the same thing: they encode visual images into a
signal which can be decoded as an array of pixels which provides the audience
the illusion of moving pictures. Strictly speaking, certain codecs have
characteristics and quirks that might make you prefer one to another either
artistically (for instance, maybe you prefer one codec's handling of a shallow
depth of field, or how another handles shadows) or technically (maybe you need
a codec's streaming ability), but these are usually opinions and not things you
can find in technical specifications. Don't get too caught up trying to
determine which codec is ";}i:2;i:9220;}i:207;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:9938;}i:208;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:"best";}i:2;i:9939;}i:209;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:9943;}i:210;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:293:" for a specific video you need to process.
Eventually you'll form your own opinions on the subject, but until you have
enough experience to intelligently form those opinions, just settle for the
codec that best suits your real world needs, and start learning how to process
video into it well.";}i:2;i:9944;}i:211;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:10237;}i:212;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:10237;}i:213;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:184:"Ffmpeg chooses what codec to use if you keep your command simple, so if you
transcode a file from one format to another, you can just define your codec
parameters with the file format:";}i:2;i:10239;}i:214;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:10423;}i:215;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:39:"$ ffmpeg -i sintel_1080p.mkv sintel.mov";}i:2;i:10425;}i:216;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:10425;}i:217;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:205:"From that command, ffmpeg converts a .mkv to a .mov format along with codec
choices that make sense for Quicktime decoders. This abstracts choice away from
you, saving you from potentially illegal [out of ";}i:2;i:10469;}i:218;a:3:{i:0;s:7:"acronym";i:1;a:1:{i:0;s:4:"spec";}i:2;i:10674;}i:219;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:96:"] choices, although if you
know your formats and codecs well enough then define them explicitly:";}i:2;i:10678;}i:220;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:10775;}i:221;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:40:"$ ffmpeg -i DSC10049.MOV -vcodec libxvid";}i:2;i:10775;}i:222;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:10821;}i:223;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:10821;}i:224;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:28:"-acodec libvorbis output.mkv";}i:2;i:10822;}i:225;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:10850;}i:226;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:10850;}i:227;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:250:"These simple format conversions don't take into account the quality or size of
your video, but of course there are additional, important attributes of video
files, all of which ffmpeg can manipulate to change or stay as close to the
same as possible.";}i:2;i:10853;}i:228;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:11103;}i:229;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:11103;}i:230;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"Frame Size";}i:2;i:11105;}i:231;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:11115;}i:232;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:11115;}i:233;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:358:"To reduce the amount of data in a video file, you can reduce the array of
pixels it contains. In other words, reduce the frame size. It's not rocket
science to understand why a smaller frame size would produce a smaller file;
1920 pixels by 1080 pixels means that the luma and chroma values of 2,073,600
pixels are being individually managed by your computer";}i:2;i:11117;}i:234;a:3:{i:0;s:6:"entity";i:1;a:1:{i:0;s:3:"...";}i:2;i:11475;}i:235;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:119:"30 times (or sometimes
more) per second, so reducing the volume of pixels reduces the file size by
orders of magnitude.";}i:2;i:11478;}i:236;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:11597;}i:237;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:11597;}i:238;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:108:"The -s flag denotes frame size in an ffmpeg command and takes as its argument a
literal pixel count such as ";}i:2;i:11599;}i:239;a:3:{i:0;s:14:"multiplyentity";i:1;a:2:{i:0;s:4:"1920";i:1;s:4:"1080";}i:2;i:11707;}i:240;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:4:" or ";}i:2;i:11716;}i:241;a:3:{i:0;s:14:"multiplyentity";i:1;a:2:{i:0;s:4:"1280";i:1;s:3:"720";}i:2;i:11720;}i:242;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:142:" or by common names such as
hd1080, hd720, vga, and so on. Here is an example of a command that converts
the format and slashes the file size:";}i:2;i:11728;}i:243;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:11870;}i:244;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:37:"$ ffmpeg -i sintel_1080p.mkv -s hd720";}i:2;i:11872;}i:245;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:11872;}i:246;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"sintel.mov";}i:2;i:11913;}i:247;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:11923;}i:248;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:11923;}i:249;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:434:"The eternal question is how much you are willing to sacrifice quality for file
size. You could reduce the frame size until your video is down to a more
manageable file size, but any full-screen playback would then require software
to up-res more and more, and screen resolutions of monitors on the market are,
of course, continually increasing. Luckily, the frame size isn't the only
method of reducing the size of the resultant file.";}i:2;i:11925;}i:250;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:12359;}i:251;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:12359;}i:252;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"Bit Rate";}i:2;i:12361;}i:253;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:12369;}i:254;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:12369;}i:255;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:371:"Rather than reducing the number of pixels contained in an image, you can reduce
the quality of how the original pixels are represented. This is the bit rate of
a video and it is the potential maximum amount of data that the video will
allot to re-creating your image, per second. It is generally defined in either
kilobits per second (Kbps) or megabits per second (Mbps).";}i:2;i:12371;}i:256;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:12742;}i:257;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:12742;}i:258;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:289:"Think of all the advantages that Blu-ray, for instance, has over DVD, as a way
to visualize the impact that bit rate has on video. Blu-ray movies have a
larger frame size, and yet they are clearer and sharper than DVDs, the textures
and objects in the background are less likely to become ";}i:2;i:12744;}i:259;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:13033;}i:260;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"muddy";}i:2;i:13034;}i:261;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:13039;}i:262;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:127:", there's no
accidental posterization in the extreme dark areas of the image, and there are
fewer digital artifacts in general.";}i:2;i:13040;}i:263;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:13167;}i:264;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:13167;}i:265;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:216:"To decide what bit rate to use for your own video, you should first know its
current bit rate, which you'll get from that initial ffmpeg -i (or mediainfo or
video-meta) command you ran against your source video file.";}i:2;i:13169;}i:266;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:13385;}i:267;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:13385;}i:268;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:222:"Since finding out the bit rate is simple enough, arbitrarily deciding upon a
lower bit rate to reduce the file size is easy. But how do you make an informed
decision about bit rate? First, you must understand how encoders ";}i:2;i:13387;}i:269;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:13609;}i:270;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"see";}i:2;i:13610;}i:271;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:13613;}i:272;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:60:" video.
It's certainly not how our human eyes perceive them.";}i:2;i:13614;}i:273;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:13674;}i:274;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:13674;}i:275;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"I,B,P Frames";}i:2;i:13676;}i:276;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:13688;}i:277;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:13688;}i:278;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"The term ";}i:2;i:13690;}i:279;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:13699;}i:280;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"frames";}i:2;i:13700;}i:281;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:13706;}i:282;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:338:" in video is misleading, even when speaking of Progressive
(1080p, 720p) video. On celluloid film there are literal frames; one new image
per 1/24th (or 25/th) of a second. The human eye can see each frame, and on a
traditional film editing bench they do in fact look at each frame as they
decide where to make a splice. Those are frames.";}i:2;i:13707;}i:283;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:14045;}i:284;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:14045;}i:285;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:503:"With video, data-complete frames (ie, a complete coherent picture) don't need
to happen every 1/24th of a second, since a computer can retain pixels that
don't change from previous frames. Imagine a video of a university professor
standing in front of a whiteboard giving a lecture, in which half the
whiteboard never changes and nothing ever crosses in front of it; the encoder
could theoretically draw it once at the beginning of the video and never
re-draw it for the full three-hours of the lecture.";}i:2;i:14047;}i:286;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:14550;}i:287;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:14550;}i:288;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:59:"An encoder may borrow pixels from previous or future frames";}i:2;i:14552;}i:289;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:14611;}i:290;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:14611;}i:291;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:41:"In ffmpeg, the frequency of high quality ";}i:2;i:14613;}i:292;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:14654;}i:293;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"intra";}i:2;i:14655;}i:294;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:14660;}i:295;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:340:" frames (I-frames) is
controlled with the GOP setting (Group of Pictures). Intuition might lead you
to believe that a video consisting exclusively of I-frames would be a lossless
video, but in practise it tends to waste bit rate with little or no gain in
quality. This is because effective bit budgeting depends not upon whether a
frame is ";}i:2;i:14661;}i:296;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:15001;}i:297;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"complete";}i:2;i:15002;}i:298;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:15010;}i:299;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:73:" but how many individual pixels are being calculated and
drawn on screen.";}i:2;i:15011;}i:300;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:15084;}i:301;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:15084;}i:302;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:452:"If the encoder can borrow pixels from the previous or next frame and save
bandwidth by not re-drawing those pixels, then a forced I-frame that
necessarily re-draws all pixels would actually demand maximum bitrate for that
frame either at the expense of the next few frames, or the overall file size.
Alternatively, the encoder can blob a group of similar pixels together and,
essentially, draw three or 10 or 30 pixels for the price of a single sample.";}i:2;i:15086;}i:303;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:15538;}i:304;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:15538;}i:305;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:452:"More important is the timing of the I-frames, and this would be a tedious thing
for us to control; luckily it's one of the functions of a video encoder. An
encoder compares frames and determine what would benefit most from a full
re-draw (producing an I-frame), what would benefit from borrowing pixels from
the previous frame (producing a P-frame(, and what would benefit from borrowing
pixels from both the previous and the following frame (B-frame).";}i:2;i:15540;}i:306;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:15992;}i:307;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:15992;}i:308;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:417:"Most of the modern encoders like ogg (theora), vpx (webm), xvid, and x.26? are
very good at setting GOP sizes so that the I-frames are intelligently placed
throughout the video, so under normal circumstances most people will never need
to set the GOP size. But if you are having problems with your results, then you
might need to alter the GOP size as part of your command. For instance, if your
encoded video is too ";}i:2;i:15994;}i:309;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:16411;}i:310;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:6:"blocky";}i:2;i:16412;}i:311;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:16418;}i:312;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:234:", then you need more I-frames so that there are
more frequent high-quality images to sample. If your encoded video is still too
big (in file size) then you might need to decrease the frequency of I-frames by
increasing the gop number.";}i:2;i:16419;}i:313;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:16653;}i:314;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:16653;}i:315;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:153:"If you want to try different GOP sizes, you might want to start with your frame
rate multiplied by 10. The GOP size is set with the -g flag. For example:";}i:2;i:16655;}i:316;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:16808;}i:317;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:16808;}i:318;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:52:" $ ffmpeg -i foo.avi -s vga -g
300 -v:b 2500 foo.mkv";}i:2;i:16810;}i:319;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:16862;}i:320;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:16862;}i:321;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:108:"A GOP size of 300 at a frame rate of 29.97 would instatiate an I-Frame every 10
seconds (300 divided by 30).";}i:2;i:16864;}i:322;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:16972;}i:323;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:16972;}i:324;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:276:"If that is too infrequent, decrement multiples of your frame rate until you hit
on something that works for you. Then take note of what you use, and what kind
of video file it was (original codec, destinatior codec, fast or slow paced
content, and so on) for future reference.";}i:2;i:16974;}i:325;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:17250;}i:326;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:17250;}i:327;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:31:"Variable and Constant Bit Rates";}i:2;i:17252;}i:328;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:17283;}i:329;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:17283;}i:330;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:695:"A computer is particularly well suited for determining just how much each of a
few million pixels has changed from one fraction of a second to the next, so
one of the best ways to decide a bit rate is to just leave it up to the
encoder. The -qscale flag in ffmpeg provides a variable bit rate (VBR), a
powerful option that lets the encoder decide what bit rate to use depending on
the complexity of some set of frames. In an action film, for example, the bit
rate would be kept low during an expository conversation in a quiet restaurant
wherein groups of pixels can be re-used for seconds at a time, but it would be
boosted once the conversation inevitably turns into an explosive action scene.";}i:2;i:17285;}i:331;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:17980;}i:332;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:17980;}i:333;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:407:"Set to 1, -qscale tells the encoder to maintain excellent overall quality,
whatever bit rate it takes. Conversely, setting -qscale to 31 tells the encoder
to allow quality to suffer. Depending on what you choose between these two
extremes, the file size savings may not be enough for your purposes. In that
case, you might need to hard-code the bit rate, which turns out to be more of
an art than a science.";}i:2;i:17982;}i:334;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:18389;}i:335;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:18389;}i:336;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:132:"While overall quality does have a relationship between frame size and bit rate
(the larger the frame size, the greater the bit rate ";}i:2;i:18391;}i:337;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:18523;}i:338;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"wants";}i:2;i:18524;}i:339;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:18529;}i:340;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:113:" to be), there is
no algorithm for the process because bit rate is also bound to pixel activity
within the frame.";}i:2;i:18530;}i:341;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:18643;}i:342;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:18643;}i:343;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:229:"A three-hour college lecture will look fine at a lower bit rate while a
90-minute action film would suffer under the same rate. This isn't only because
of the difference in pixel activity; there's an artistic quality to defining
";}i:2;i:18645;}i:344;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:18874;}i:345;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"quality";}i:2;i:18875;}i:346;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:18882;}i:347;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:162:". Your eye is more discerning of the action film; most people are a
lot less forgiving of digital artifacts in their entertainment than in a boring
lecture video.";}i:2;i:18883;}i:348;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:19045;}i:349;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:19045;}i:350;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:692:"So it comes back to knowing your video. Skim through your video and make some
general classifications; is it fast-paced action video, or an educational
lecture, or a travel video, a student film or a million-dollar blockbuster, or
a family vacation? Let this guide you toward what range of bit rates to
consider. It helps to think in familiar terms like below-DVD-quality,
DVD-quality, or Blu-ray Quality. For below-DVD-quality, start in the
high-hundreds for standard definition video or the low thousands for high
definition video. For DVD-quality, start at 7000kbps or 8000kbps for standard
definition and 12000kbps to 15000kbps for HD. For Blu-ray quality, look at
25000kbps to 35000kbps.";}i:2;i:19047;}i:351;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:19739;}i:352;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:19739;}i:353;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:69:"A command providing a high quality VBR encode with reduced file size:";}i:2;i:19741;}i:354;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:19811;}i:355;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:37:"$ ffmpeg -i sintel_1080p.avi -s hd720";}i:2;i:19811;}i:356;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:19811;}i:357;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:33:"-aspect 16:9 -qscale 1 sintel.mkv";}i:2;i:19852;}i:358;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:19885;}i:359;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:19885;}i:360;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:98:"Or in the case that you require more control over bit rate, you might specify a
constant bit rate:";}i:2;i:19887;}i:361;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:19985;}i:362;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:19985;}i:363;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:73:" $ ffmpeg -i sintel_1080p.avi -s hd720
-aspect 16:9 -v:b 15000 sintel.mkv";}i:2;i:19987;}i:364;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:20060;}i:365;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20060;}i:366;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:200:"Notice that in both examples the size is reduced from the original 1080p to
720p. This, combined with lowering the bitrates from the original video,
compresses the file to a more reasonable file size.";}i:2;i:20062;}i:367;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:20262;}i:368;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20262;}i:369;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:128:"If you were only transcoding and were trying to achieve zero loss of quality,
then a simpler and less specific command would do;";}i:2;i:20264;}i:370;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:20392;}i:371;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:49:"$ ffmpeg -i sintel_1080p.avi -qscale 1 sintel.mkv";}i:2;i:20394;}i:372;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20394;}i:373;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:15:"2-pass Encoding";}i:2;i:20448;}i:374;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:20463;}i:375;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20463;}i:376;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:435:"One way to help the encoder achieve the best possible quality at the best
possible file size is to use 2-pass encoding. The first pass analyses your
video and creates a log file which ffmpeg can use during the second pass when
it actually encodes video and audio. A 2-pass encode doesn't mean your file
will be any smaller, necessarily, but it does mean that the encoding will be
better and possibly more efficient with budgeting bits.";}i:2;i:20465;}i:377;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:20900;}i:378;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20900;}i:379;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:74:"To perform a 2-pass encode, the first pass must be exclusively analytical:";}i:2;i:20902;}i:380;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:20977;}i:381;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:75:"$ ffmpeg -i sintel.mkv -vcodec libxvid -an
-pass 1 -f rawvideo -y /dev/null";}i:2;i:20977;}i:382;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:20977;}i:383;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:303:"This causes ffmpeg to write the new video to /dev/null (in other words, it
throws the results out) while writing data about the frames to a file called
ffmpeg2pass, saved in the current directory. Since audio is not accounted for
during this process, you can use the -an flag to ignore the audio stream.";}i:2;i:21059;}i:384;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:21362;}i:385;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:21362;}i:386;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:184:"The second pass is performed like your usual ffmpeg command, with the addition
of the -pass 2 flag and the name of the log file to which ffmpeg should refer: 
-passlogfile ffmpeg2pass.";}i:2;i:21364;}i:387;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:21549;}i:388;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:149:"$ ffmpeg -i sintel.mkv -vcodec libxvid -acodec
libvorbis -r 18 -ar 44100 -b:a 80k -qscale 10 -s vga -pass 2
-passlogfile ffmpeg2pass sintel_small.mkv";}i:2;i:21549;}i:389;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:21549;}i:390;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"Frame Rate";}i:2;i:21707;}i:391;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:21717;}i:392;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:21717;}i:393;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:425:"Any video has a natural frame rate, depending on how it was recorded. As with
everything else, you can find out a video's native frame rate with ffmpeg -i or
a tool like mediainfo or vlc. Common values are 29.97 for standard definition
videos, 23.98 for DVD, and 24, 48, and 60 for high definition. As you might
expect, reducing the frame rate will reduce the resulting file size and
increase the streamability of your video.";}i:2;i:21719;}i:394;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:22144;}i:395;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:22144;}i:396;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:522:"The pay-off with a reduced frame rate is mostly aesthetic; the motion is not as
smooth as with a video's native frame rate. The more you reduce the frame rate
the more drastic and noticeable this becomes. Exactly when you or your audience
actually start to notice is an entirely different matter and depends on how
much movement there is in the frame in the first place, and whether or not the
viewer tends to notice things like that. Experiment with different frame rates
to witness the practical difference between them.";}i:2;i:22146;}i:397;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:22668;}i:398;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:22668;}i:399;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:410:"In practise, lowering the frame rate does not reduce the file size as much as
you might think (taking a 60 fps video down to 30 with no other change, for
instance, does not reduce the file size by half as you might expet). It's
usually safe to leave the frame rate at its native value, or at 24 or so in the
cases of high frame rate source files, unless your destination device or
application demands a change.";}i:2;i:22670;}i:400;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:23080;}i:401;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:23080;}i:402;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:52:"Frame rate is controlled in ffmpeg with the -r flag:";}i:2;i:23082;}i:403;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:23134;}i:404;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:23134;}i:405;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:40:" $ ffmpeg -i foo.mkv -r 18
foo_18fps.mkv";}i:2;i:23136;}i:406;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:23176;}i:407;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:23176;}i:408;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:5:"Audio";}i:2;i:23178;}i:409;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:23183;}i:410;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:23183;}i:411;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:200:"Since most videos have sound, at least some portion of your overall file size
is determined by how its audio has been encoded. The same principles apply to
audio, with a few variations in terminology.";}i:2;i:23185;}i:412;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:23385;}i:413;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:23385;}i:414;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:436:"Audio has a bit rate, assigned with the -b:a flag, which determines how much
data is used to recreate the audio waves. You're probably already familiar with
this idea since online music stores usually advertises their song quality as
either 128kbps or the higher quality 192kbps or 256kbps versions. The higher
bit rates usually provide better subtleties, with more modest ranges (128kbps
is a good middle-of-the-road number) provide a ";}i:2;i:23387;}i:415;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:23823;}i:416;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"good enough";}i:2;i:23824;}i:417;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:23835;}i:418;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:261:" quality, while the
lower ranges start to noticeably sacrifice quality. Once again, how much this
matters to you depends on you, and the content of the video itself. A lecture
video can tolerate 80kbps encoding while a video with a lush soundtrack would
suffer.";}i:2;i:23836;}i:419;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:24097;}i:420;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:24097;}i:421;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:324:"Audio also has channels. As you might expect, the more channels you have, the
larger the file will be. It's common practise to reduce any surround sound
soundtrack to stereo, and in some cases to simply use one mono channel. ffmpeg
uses the -c flag to define how many channels you want, with 2 being stereo and
1 being mono.";}i:2;i:24099;}i:422;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:24423;}i:423;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:24423;}i:424;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:548:"The sample rate of audio defines how many samples of a soundwave is used per
second, and is measured in thousands of samples per second. DVD quality is
considered 48000hz while CD quality is 44100hz. Anything lower (32000hz,
22050hz, 16000hz) suffers noticeably in quality although they do have
remarkable results on file size savings. However, when transcoding, changing
the sample rate of the audio drastically could throw the audio track out of
sync with your video, so use this ability carefully. ffmpeg uses the -ar flag
to define sample rate.";}i:2;i:24425;}i:425;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:24973;}i:426;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:24973;}i:427;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:83:"Here is an example of defining the type of sound encoding during video
compression:";}i:2;i:24975;}i:428;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:25058;}i:429;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:25058;}i:430;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:84:" $ ffmpeg -i sintel_1080p.avi -s hd720
-b:v 8000 -b:a 128k -ar 44100 -c 1 sintel.mkv";}i:2;i:25060;}i:431;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:25144;}i:432;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:25144;}i:433;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"Threads";}i:2;i:25146;}i:434;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:25153;}i:435;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:25153;}i:436;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:109:"If you're using a computer with multiple CPU cores, you can take advantage of
the -threads flag. It's simple:";}i:2;i:25155;}i:437;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:25265;}i:438;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:44:"$ ffmpeg -i sintel.mkv -threads 8 sintel.mov";}i:2;i:25265;}i:439;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:25265;}i:440;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:27:"How to Test Before Encoding";}i:2;i:25314;}i:441;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:25341;}i:442;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:25341;}i:443;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:305:"Because a computer is doing the work, many people view video encoding as a
strictly technical process. And certainly, the encoders we use should be
respected; no sane artist or programmer would want to decide how each
individual frame will be encoded when there 30 of them per second, per 90
minute movie.";}i:2;i:25343;}i:444;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:25648;}i:445;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:25648;}i:446;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:650:"On the other hand, there's a lot of artistry in looking at a video and making
intelligent choices when issuing the encoding commands to the computer. Take
into account what the video content is, how people will be watching it, what
action is happening within the frames and what qualities are important. Use
these artistic impressions to guide you in the choices you make about frame
size, bitrate, and frame rate. Encode with two passes, and encode multiple
versions of the same video. Compare the results. In no time, you'll get a good
feeling for what different codecs have to offer, what kinds of videos can
handle different kinds of compression.";}i:2;i:25650;}i:447;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:26300;}i:448;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:26300;}i:449;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:193:"It's not easy to test encoding when each encode takes 6 hours, only to be
thrown out for another try. Luckily, ffmpeg has a start and end time, allowing
you to encode small sections of a video.";}i:2;i:26302;}i:450;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:26495;}i:451;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:26495;}i:452;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:321:"The -ss option dictates what time to start encoding, and the -t dictates how
long to encode for (not the timecode at which to stop, as a video editor would
expect; the values are start time and duration, not in and out). For example,
to start encoding at 3 minutes and 30 seconds into a video, and to encode for 1
minute:";}i:2;i:26497;}i:453;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:26819;}i:454;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:75:"$ ffmpeg -i sintel.mov -ss 00:03:30:00 -t 00:01:00:00 -threads 8 sintel.mkv";}i:2;i:26819;}i:455;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:26819;}i:456;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:98:"Run a few hundred encoding tests overnight, study the results, and you'll be an
expert in no time.";}i:2;i:26899;}i:457;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:26997;}i:458;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:26999;}i:459;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:26999;}}
a:104:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:0;}i:2;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:23:"Chapter 1. Lightworks";}i:2;i:1;}i:3;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:24;}i:4;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:24;}i:5;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Strengths";}i:2;i:26;}i:6;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:35;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:35;}i:8;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"Powerful";}i:2;i:37;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:45;}i:10;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:265:"All the essential video editing functions are present, in a professional
environment; in and out points, clip monitors and dedicated timeline
monitors, copious effects, clip bins, splicing, sliding, slipping, titling,
a full proxy system, auto saves, and much more.";}i:2;i:45;}i:11;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:328;}i:12;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:328;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"Industry-Grade";}i:2;i:330;}i:14;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:344;}i:15;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:494:"If you are used to working in the industry on professianl editing
workstations or a Steenbeck flatbed editor, this is unquestionably what you
are seeking. Lightworks takes an editing suite and puts it into your Linux
box, complete with timecode- and track- based editing styles, extensive
media management options, stable and high-quality effects, EDL import and
export, support for Final Cut and Avid format exchanges, and an over-all
lack of bloat that distracts from its streamlined purpose.";}i:2;i:344;}i:16;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:868;}i:17;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:868;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:13:"Documentation";}i:2;i:870;}i:19;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:883;}i:20;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:96:"Very good video tutorials for Lightworks are available from Editshare, for
free, on youtube.com.";}i:2;i:883;}i:21;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:989;}i:22;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:989;}i:23;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:9:"Stability";}i:2;i:991;}i:24;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1000;}i:25;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:164:"Lightworks is focused and streamlined, tried and proven. It is a stable
editor, so much so that it is nearly a kiosk. Start it, and live in its
environment all day.";}i:2;i:1000;}i:26;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:1178;}i:27;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1178;}i:28;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:10:"Weaknesses";}i:2;i:1180;}i:29;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1190;}i:30;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1190;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:14:"Open Source..?";}i:2;i:1192;}i:32;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1206;}i:33;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:700:"Lightworks has been around for three decades and has been used on a wide
variety of films, and although it was announced that it would be open
sourced a few years ago, the actual source code itself has so far not
surfaced. If you are only interested in finding a company in which you can
have confidence that it will not deprecate and abandon your project file
formats for years to come, then Lightworks is as good as some closed source
NLEs and quite a lot better than others. If you are looking far on open
source solution, then this is not yet the solution for you, although it is
still on the official Lightworks roadmap to formally release the codebase
(no word yet on what license it will use).";}i:2;i:1206;}i:34;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:1948;}i:35;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1948;}i:36;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"Complex";}i:2;i:1950;}i:37;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1957;}i:38;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:118:"If you are not used to traditional film editing, Lightworks may confuse you
initially. There will be a learning curve.";}i:2;i:1957;}i:39;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:2085;}i:40;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2085;}i:41;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:12:"Registration";}i:2;i:2087;}i:42;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2099;}i:43;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:95:"Registration is required to use Lightworks. It is free to register, but you
do have to sign in.";}i:2;i:2099;}i:44;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:2204;}i:45;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2204;}i:46;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:26:"Conservatively Restrictive";}i:2;i:2206;}i:47;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2232;}i:48;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:301:"Although it has full access to the same robust multimedia libraries that
everything else does, Lightworks only permits you to import a set of
codecs, and export even a smaller set. This makes it slightly more
predictable than an NLE that lets anything in, but it is a little
restrictive by comparison.";}i:2;i:2232;}i:49;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:2555;}i:50;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2555;}i:51;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"64-bit Only";}i:2;i:2557;}i:52;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2568;}i:53;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:113:"Currently, Lightworks is for 64-bit architecture only, so if you are
running a 32-bit install, you cannot run it.";}i:2;i:2568;}i:54;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:2691;}i:55;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:2691;}i:56;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:11:"Heavyweight";}i:2;i:2693;}i:57;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:2704;}i:58;a:3:{i:0;s:12:"preformatted";i:1;a:1:{i:0;s:466:"Lightworks is not a lightweight or simple application. It officially
requires an Nvidia GPU running proprietary drivers (in practise, it can be
run on a good Intel graphics chipset, although it performs better on a
proper GPU), at least 3GB RAM (you want at least 8GB in practise), and even
suggests running the application from a separate drive than where your
media is located to minimise lag. It is intended to be an edititng station,
not a lightweight video app.";}i:2;i:2704;}i:59;a:3:{i:0;s:6:"plugin";i:1;a:4:{i:0;s:20:"markdowku_codeblocks";i:1;b:1;i:2;i:4;i:3;s:1:"
";}i:2;i:3200;}i:60;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3200;}i:61;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:8:"See Also";}i:2;i:3202;}i:62;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3210;}i:63;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3210;}i:64;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:26:"Kdenlive
Blender
Flowblade";}i:2;i:3212;}i:65;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3238;}i:66;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3238;}i:67;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:249:"Lightworks is a video editing application aimed at the professional video
editor. It is designed with a traditional film editing environment in mind and
has been compared to non-digital platforms like the Steenbeck. While some NLE
systems encourage ";}i:2;i:3240;}i:68;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:3489;}i:69;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"lifting";}i:2;i:3490;}i:70;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:3497;}i:71;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:489:" clips out of the edit to adjust them, or conversely
to drop all clips into the timeline and use that space as a work table, 
Lightworks treats the timeline as a working draft of the completed project, and
encourages users to unjoin splices just as one would unjoin splicing tape on
real film, and adjust clips with slide and roll edits. It can be an efficient
and technical way of editing, but to people who learnt editing by trial and
error, it there is, realistically, a learning curve.";}i:2;i:3498;}i:72;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:3987;}i:73;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:3987;}i:74;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:605:"The main advantage to Lightworks for the Linux user is that it is, all else
being equal, inarguably the most polished and reliable option in terms of a
complete professional editor. If you are looking for an NLE that works, which
you can install and move on with your life, then Lightworks, combined with
capable hardware, will put an end to your search. Lesser NLE-for-Linux attempts
pale in comparison; it is probably the only NLE on Linux that will convince a
doubtful prospective user that Linux can be a serious video editing solution
(albeit in part by sheer intimidation of the application itself).";}i:2;i:3989;}i:75;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4594;}i:76;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4594;}i:77;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:301:"This is not a sales pitch for Lightworks, however, and you should use whatever
suits your own workflow. In fact, Lightworks is not quite open source yet
(there is an as-yet undelivered promise to release its code), so if you want to
use only free and open source software, then opt for something else.";}i:2;i:4596;}i:78;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4897;}i:79;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4897;}i:80;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:92:"To install Lightworks, use Slackermedia's SlackBuild script, available from
slackbuilds.org.";}i:2;i:4899;}i:81;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:4991;}i:82;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:4991;}i:83;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:288:"It is required that you create a Lightworks account and sign into Lightworks in
order to run the application, so make sure you at least launch Lightworks with
an internet connection after installing it. After this initial launch, you do
not need internet access for Lightworks ever again.";}i:2;i:4993;}i:84;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5281;}i:85;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5281;}i:86;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:7:"Warning";}i:2;i:5283;}i:87;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5290;}i:88;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5290;}i:89;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:365:"Editshare, the owner and proprietor of Lightworks, emails a newsletter to all
account holders by default, and obviously Slackermedia cannot guarantee that
they do not sell email addresses to advertisers. You may want to use a
junk-mail email address or an alias account or a temporary address from a
service like 10minutemail.com, or whatever you do to manage spam.";}i:2;i:5292;}i:90;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5657;}i:91;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5657;}i:92;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:52:"Lightworks can be used for free, but there are also ";}i:2;i:5659;}i:93;a:3:{i:0;s:18:"doublequoteopening";i:1;a:0:{}i:2;i:5711;}i:94;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:3:"pro";}i:2;i:5712;}i:95;a:3:{i:0;s:18:"doublequoteclosing";i:1;a:0:{}i:2;i:5715;}i:96;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:273:" features (user
defined project locations, sharing, timeline rendering, stereoscopic output,
advanced export options) available for either a monthly subscription fee or for
yearly or version-buyout plans. For comparisons between the different support
options, see lwks.com.";}i:2;i:5716;}i:97;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:5989;}i:98;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:5989;}i:99;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:102:"A very comprehensive set of tutorials from Editshare are available for free on
their YouTube channel, ";}i:2;i:5991;}i:100;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:34:"https://youtube.c/m/user/editshare";i:1;N;}i:2;i:6093;}i:101;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:".";}i:2;i:6127;}i:102;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:6128;}i:103;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:6128;}}
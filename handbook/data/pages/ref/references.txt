This is but a small sample of Linux-related articles within the multimedia space. Since most readers do not read articles about their favourite artists to learn about the technical tools they use, the articles are not as prolific as they "should" be, but enough searching online renders a fair sample.

  -[[http://www.digitalproducer.com/article/DreamWorks-Animations-Kung-Fu-Panda-created-with-Linux-based-HP-workstations-415193|Dreamworks Interview]]
  -Pixar's [[http://www.muktware.com/2013/04/pixar-animation-studios-uses-red-hat-enterprise-linux/4271|OpenSubDiv Demo]]
  -[[http://renderman.pixar.com/resources/current/rps/installLinux.html|Renderman Docs]]
  -[[http://www.olivier-ladeuix.com/blog/2013/04/28/pixar-animation-software|Presto]] animation software by Pixar
  -[[http://www.linuxjournal.com/magazine/emphasisthe-day-earth-stood-stillemphasis|Linux Journal article about Weta]]
  -[[http://blog.dustinkirkland.com/2010/01/39000-core-ubuntu-cluster-renders.html|Avatar]] renderwall
  -[[http://www.creativeplanetnetwork.com/news/news-articles/linux-hollywood/381003|Creative Planet]]
  -[[http://asperasoft.com/customers/customer/view/Customer/show/weta-digital|Aspera]]
  -[[http://www.pcmag.com/article2/0,2817,1046864,00.asp|PC Magazine]]
  -[[http://archive09.linux.com/feature/22473|Linux at Dreamworks]] in 2002
  -[[http://www.openexr.com|OpenEXR]] by ILM and Weta Digital]]
  -[[http://www.fxguide.com/featured/manuka-weta-digitals-new-renderer|Manuka]] by Weta Digital
Chapter 1. Lightworks

Strengths

Powerful

    All the essential video editing functions are present, in a professional
    environment; in and out points, clip monitors and dedicated timeline
    monitors, copious effects, clip bins, splicing, sliding, slipping, titling,
    a full proxy system, auto saves, and much more.

Industry-Grade

    If you are used to working in the industry on professianl editing
    workstations or a Steenbeck flatbed editor, this is unquestionably what you
    are seeking. Lightworks takes an editing suite and puts it into your Linux
    box, complete with timecode- and track- based editing styles, extensive
    media management options, stable and high-quality effects, EDL import and
    export, support for Final Cut and Avid format exchanges, and an over-all
    lack of bloat that distracts from its streamlined purpose.

Documentation

    Very good video tutorials for Lightworks are available from Editshare, for
    free, on youtube.com.

Stability

    Lightworks is focused and streamlined, tried and proven. It is a stable
    editor, so much so that it is nearly a kiosk. Start it, and live in its
    environment all day.

Weaknesses

Open Source..?

    Lightworks has been around for three decades and has been used on a wide
    variety of films, and although it was announced that it would be open
    sourced a few years ago, the actual source code itself has so far not
    surfaced. If you are only interested in finding a company in which you can
    have confidence that it will not deprecate and abandon your project file
    formats for years to come, then Lightworks is as good as some closed source
    NLEs and quite a lot better than others. If you are looking far on open
    source solution, then this is not yet the solution for you, although it is
    still on the official Lightworks roadmap to formally release the codebase
    (no word yet on what license it will use).

Complex

    If you are not used to traditional film editing, Lightworks may confuse you
    initially. There will be a learning curve.

Registration

    Registration is required to use Lightworks. It is free to register, but you
    do have to sign in.

Conservatively Restrictive

    Although it has full access to the same robust multimedia libraries that
    everything else does, Lightworks only permits you to import a set of
    codecs, and export even a smaller set. This makes it slightly more
    predictable than an NLE that lets anything in, but it is a little
    restrictive by comparison.

64-bit Only

    Currently, Lightworks is for 64-bit architecture only, so if you are
    running a 32-bit install, you cannot run it.

Heavyweight

    Lightworks is not a lightweight or simple application. It officially
    requires an Nvidia GPU running proprietary drivers (in practise, it can be
    run on a good Intel graphics chipset, although it performs better on a
    proper GPU), at least 3GB RAM (you want at least 8GB in practise), and even
    suggests running the application from a separate drive than where your
    media is located to minimise lag. It is intended to be an edititng station,
    not a lightweight video app.

See Also

Kdenlive
Blender
Flowblade

Lightworks is a video editing application aimed at the professional video
editor. It is designed with a traditional film editing environment in mind and
has been compared to non-digital platforms like the Steenbeck. While some NLE
systems encourage "lifting" clips out of the edit to adjust them, or conversely
to drop all clips into the timeline and use that space as a work table, 
Lightworks treats the timeline as a working draft of the completed project, and
encourages users to unjoin splices just as one would unjoin splicing tape on
real film, and adjust clips with slide and roll edits. It can be an efficient
and technical way of editing, but to people who learnt editing by trial and
error, it there is, realistically, a learning curve.

The main advantage to Lightworks for the Linux user is that it is, all else
being equal, inarguably the most polished and reliable option in terms of a
complete professional editor. If you are looking for an NLE that works, which
you can install and move on with your life, then Lightworks, combined with
capable hardware, will put an end to your search. Lesser NLE-for-Linux attempts
pale in comparison; it is probably the only NLE on Linux that will convince a
doubtful prospective user that Linux can be a serious video editing solution
(albeit in part by sheer intimidation of the application itself).

This is not a sales pitch for Lightworks, however, and you should use whatever
suits your own workflow. In fact, Lightworks is not quite open source yet
(there is an as-yet undelivered promise to release its code), so if you want to
use only free and open source software, then opt for something else.

To install Lightworks, use Slackermedia's SlackBuild script, available from
slackbuilds.org.

It is required that you create a Lightworks account and sign into Lightworks in
order to run the application, so make sure you at least launch Lightworks with
an internet connection after installing it. After this initial launch, you do
not need internet access for Lightworks ever again.

Warning

Editshare, the owner and proprietor of Lightworks, emails a newsletter to all
account holders by default, and obviously Slackermedia cannot guarantee that
they do not sell email addresses to advertisers. You may want to use a
junk-mail email address or an alias account or a temporary address from a
service like 10minutemail.com, or whatever you do to manage spam.

Lightworks can be used for free, but there are also "pro" features (user
defined project locations, sharing, timeline rendering, stereoscopic output,
advanced export options) available for either a monthly subscription fee or for
yearly or version-buyout plans. For comparisons between the different support
options, see lwks.com.

A very comprehensive set of tutorials from Editshare are available for free on
their YouTube channel, https://youtube.c/m/user/editshare.


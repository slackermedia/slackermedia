Chapter 1.  HandBrake

See Also

dvd::rip
k3b

HandBrake is a commandline and GUI DVD ripper that will read DVDs and blu-ray
and convert them to some other format, like Ogg Theora, webm, xvid, x264, and
others. It is one of the many invaluable tools for the video editor, who can
typically expect to receive video from every imaginable source.

HandBrake can also be used as a simple format converter, transcoding one file
into another format.

HandBrake is available as a binary install from slackware.org.uk/people/alien/
restricted_slackbuilds/handbrake or you can compile your own copy from
slackbuilds.org, although the way that the HandBrake build system works does
not make it valuable to compile from source, so Slackermedia recommends using
the binary install (in spite of the fact that the maintainer for the SlackBuild
is also the maintainer of Slackermedia).

Important

HandBrake requires very specific codec versions in order to build correctly, so
the SlackBuild downloads all of the HandBrake-sanctioned codec versions even
though you may already have all of those codecs installed. The SlackBuild is
not installing or re-installing these codecs onto your system, but using them
to compile HandBrake. Those versions of the codecs are discarded after 
HandBrake is finished building. Install the binary package instead! it's much
simpler.

Once installed, invoke HandBrake in a terminal as HandBrakeCLI and within the
GUI as only HandBrake.


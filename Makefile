# Makefile for slackermedia book
# updated for Publicanization
# updated for dePublicanization

slackermedia:
	cat ./compilingNotes/writing.xml \
	./compilingNotes/screenwriter.xml \
	./compilingNotes/screenplay-tools.xml \
	./compilingNotes/celtx.xml \
	./compilingNotes/trelby.xml \
	./compilingNotes/99.xml > ./en-US/writing-notes.tmp.xml
	cat ./compilingNotes/synths.xml > ./en-US/synth-notes.tmp.xml
	cat ./compilingNotes/00.xml \
	./compilingNotes/audacity.xml \
	./compilingNotes/blender.xml \
	./compilingNotes/digikam.xml \
	./compilingNotes/ffmpeg.xml \
	./compilingNotes/ffmpeg_howto.xml \
	./compilingNotes/fonts.xml \
	./compilingNotes/gimp-brushes.xml \
	./compilingNotes/handbrake.xml \
	./compilingNotes/kdenlive.xml \
	./compilingNotes/qtractor.xml \
	./compilingNotes/synfig-studio.xml \
	./compilingNotes/transcode.xml \
	./compilingNotes/sox.xml \
	./compilingNotes/ffmpeg2theora.xml \
	./compilingNotes/video-meta.xml \
	./compilingNotes/vlc.xml \
	./compilingNotes/99.xml > ./en-US/compiling-notes.tmp.xml
	@echo OK. Now make html, epub, or txt

html:	./en-US/writing-notes.tmp.xml ./en-US/compiling-notes.tmp.xml ./en-US/synth-notes.tmp.xml
	xmlto html -x ./xsl/html.xsl --stringparam="chunker.output.encoding"="'UTF-8'" --stringparam="use.extensions"="0" --stringparam="tablecolumns.extension"="0" ./en-US/Slackermedia.xml -o html
	cp -r ./en-US/images ./en-US/Common_Content ./html

epub:	./en-US/writing-notes.tmp.xml ./en-US/compiling-notes.tmp.xml ./en-US/synth-notes.tmp.xml
	xmllint --xinclude ./en-US/Slackermedia.xml > ./en-US/resolved.tmp.xml
	xmlto epub -x ./xsl/epub.xsl --stringparam="use.extensions"="0" en-US/resolved.tmp.xml -o epub
	zip -r ./epub/resolved.tmp.epub OEBPS/Common_Content/
	zip -r ./epub/resolved.tmp.epub OEBPS/images/
	mv ./epub/resolved.tmp.epub ./epub/slackermedia.epub

txt:	./en-US/writing-notes.tmp.xml ./en-US/compiling-notes.tmp.xml ./en-US/synth-notes.tmp.xml
	xmlto txt --stringparam="chunker.output.encoding"="'UTF-8'" --stringparam="use.extensions"="0" --stringparam="tablecolumns.extension"="0" ./en-US/Slackermedia.xml

# pdf is currently broken but i dont really build to it anymore so not motivated to fix
pdf:	./en-US/writing-notes.tmp.xml ./en-US/compiling-notes.tmp.xml ./en-US/synth-notes.tmp.xml
	xmllint --xinclude ./en-US/Slackermedia.xml > ./en-US/resolved.tmp.xml
	xmlto fo -x ./xsl/lulu.xsl \
	--stringparam page.width=6in \
	--stringparam page.height=9in \
	--stringparam body.font.family="Times" \
	--stringparam body.font.master=10 \
	--stringparam body.font.size=10 \
	--stringparam page.margin.inner=0.5in \
	--stringparam page.margin.outer=1in \
	--stringparam page.margin.top=0.75in \
	--stringparam page.margin.bottom=0.75in \
	--stringparam title.margin.left=0.2in \
	--stringparam chapter.autolabel=1 \
	./en-US/resolved.tmp.xml -o pdf
	fop pdf/resolved.tmp.fo pdf/Slackermedia.tmp.pdf
	gs -sFONTPATH=../fonts \
	-o pdf/Slackermedia+fonts.tmp.pdf \
	-sDEVICE=pdfwrite \
	-dPDFSETTINGS=/prepress \
	pdf/Slackermedia.tmp.pdf
	convert ./en-US/images/cover_slackermedia_front.png ./pdf/cover_slackermedia_front.tmp.pdf
	pdftk ./pdf/cover_slackermedia_front.tmp.pdf \
	pdf/Slackermedia+fonts.tmp.pdf \
	cat output \
	pdf/Slackermedia.pdf
	-rm -f pdf/*.tmp.pdf
	-rm -f pdf/*.fo

tidy:
	-rm -f ./en-US/*.tmp.xml
	-rm -rf ./tmp
	-rm -f ./en-US/*~

clean:
	-rm -f ./en-US/*.tmp.xml
	-rm -rf ./tmp
	-rm -f ./en-US/*~
	-rm -rf ./pdf
	-rm -rf ./epub
	-rm -rf ./html

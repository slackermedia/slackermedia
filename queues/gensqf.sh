#!/bin/bash
ARG="${1}"

#echo "you chose ${ARG}"

for SET in a v g
do
    #echo ":${SET}"
    for LINE in $( < ${ARG} )
    do
	echo ${LINE} | grep ":${SET}" | cut -f1 -d":" >> slackermedia-"${SET}".sqf
	echo ${LINE} | grep ":x" | cut -f1 -d":" >> slackermedia-"${SET}".sqf
    done
done

# generate huge
cat ${ARG} | cut -f1 -d":" >> slackermedia-huge.sqf
